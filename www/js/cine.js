/**
 * Created by xdire on 29.09.14.
 */
var cineTimeTzObj={"-3":-2,"-4":-3,"-5":-4,"-6":-5,"-7":-6,"-9":-8,"-10":-9};
var cineViewW=0;
var cineViewH=0;
var cineTOS=false;

var cineFnWindows=[];
var cineFnLastWindow;
var cineFnCurrentWindow;

var cineFnNotify=null;

var cineFnTimeObject=null;

var cineFnMenuArray;
var cineFnSidesArray;

var cinePriceListObject=null;
var cinePriceObject;
var cinePriceTitle;
var cineSideOptions;

var cineTempItemList={};

var cOrderPrice={priceCash:0,taxCash:0,tipCash:0,totalCash:0,tipPercent:0,taxPercent:0};
var astype={number:'num',string:'str'};

(function(){
    var $cine = function(data){return new cineBox(data);};
    var $cineViewportH=0;
    var $cineViewportW=0;
    var $cineFnDeviceType=false;
    var $cineFnMainObjects={
        //queryBase:'http://eatnow.xdire.ru/',queryApi:'srv',queryStat:'stat',queryExt:'ext',
        //queryBase:'http://eatnow1212/',queryApi:'srv',queryStat:'stat',queryExt:'ext',
        queryBase:'https://api.myvoila.me/',queryApi:'srv',queryStat:'stat',queryExt:'ext',
        queryResto:'restoinfo',queryOrder:'restoorder',
        methodNavi:'get_location',methodUser:'userinfo',methodCard:'cardinfo',methodForm:'cardform',methodList:'getrestolist',methodCartId:'getorderident',
        pageAnims:'cine_fn_animations',
        pageSearch:'cine_fn_form_search',pageSearchInp:'cine_fn_loc_search',
        modalDefW:320,modalDefH:64,modalClass:'cine_fn_modal_default',modalHeader:'h3',
        domLayer:'div',domButton:'a',domButtonForm:'input',
        domForm:'form',domInput:'input', domSelect:'select',
        formCenter:'cine_fn_form_centerer',
        btnClass:'cine_fn_simple_btn',mbtnClass:'cine_fn_color_btn',
        reqPrefix:'cinereq_',infoPrefix:'cineinfo_',
        bTreeLink:'https://js.braintreegateway.com/v2/braintree.js'};
    var cineBox = function(ciobject){

        this.version='0.5';
        var mainIterObj=null;
        var mainIterCout=0;

// LOADING SECTION -----------------------------------------------------
        function cineBoxFnAppLoad(state,init){

            cineViewH=window.innerHeight;
            cineViewW=window.innerWidth;

            if(state){
                if(init !== null){
                    var cine = new cineBox();
                    for(var i in init){
                        var c=init[i];

                        if(c=='page')
                            cine.init.page();
                        //if(c=='user')
                            //cine.init.userui(cine);
                        if(c=='cart')
                            cine.init.cart();
                        if(c=='resto')
                            cine.init.resto();
                    }
                }
            }
        }

        this.load = {
            sysChckTime:100,
            sysInitWith:null,
            sysChck: function(){
                mainIterCout++;
                if(mainIterCout>1000)
                    clearInterval(mainIterObj);
                if(document.readyState==="complete"){
                    clearInterval(mainIterObj);
                    cineBoxFnAppLoad(true,this.sysInitWith);
                }
            },
            sysInit: function(initwith){
                if(typeof initwith === 'string')
                    this.sysInitWith=initwith.split(',');
                mainIterObj=setInterval(this.sysChck.bind(this),this.sysChckTime);
            }
        };

// INITIALIZATION SECTION -----------------------------------------------------
        this.init = {

            page: function(){
                $cineFnDeviceType=true;
                cineIndexAfterLoad();
                //cineBoxFnAnimResize();
            },
            /*userui: function(cobj){

                var obj,r;
                var uib = document.getElementById('cine_fn_view_buttons');
                if(uib!==null) {
                    uib = uib.value;
                    uib = uib.split(',');
                    for (var u in uib) {
                        if (obj = document.getElementById(uib[u])) {
                            r = obj.getAttribute('cinereqtype');

                            if (r == 'view') {// obj.addEventListener('click',function(e){cobj.rexec.get(e);});
                            }
                            if (r == 'popup') { // obj.addEventListener('click',function(e){cobj.rexec.route(e);});
                            }
                            if (r == 'man') {// obj.addEventListener('click',function(e){cobj.rexec.getManual(e);});
                            }

                        }
                    }
                }

                if(typeof cineFnUserInit === 'function'){
                    cineFnUserInit();
                }

            }, */
            resto: function(){
                $cineViewportH=window.innerHeight;
                var view=document.getElementById('cine_fn_view');
                view.style.height=$cineViewportH+'px';
                if(typeof cineFnRestoInit === 'function'){
                    cineFnRestoInit();
                }
            },
            cart: function(){

                var url,item='{"order":0}';
                if(url = $cineFnMainObjects.queryBase){
                    url += '?query='+$cineFnMainObjects.queryOrder+'&type='+$cineFnMainObjects.methodCartId;
                    var cine = new cineBox();
                    if(window.localStorage!=='undefined'){
                        item=window.localStorage.getItem('userorder');
                    }
                    cine.ajax('post',url,{user:0,ident:item},function(msg){
                        cineBoxFnOrderCartLoad(msg);
                    });
                }

            }
        };

// AJAX SECTION -----------------------------------------------------
        this.ajaxfunc = {

            initObj: function(){
                var xhr = false;
                if (window.XMLHttpRequest){
                    xhr = new XMLHttpRequest();
                }return xhr;
            },
            postData: function(xhr,url,data,callback,backobj){
                xhr.open('POST',url,true);
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhr.onreadystatechange = function(){
                    if (xhr.readyState == 4){
                        if(xhr.status === 200)
                            callback(xhr.responseText);
                        else
                        {
                            if(xhr.status===404){
                                //callback('{"error":100,"error_msg":"Requested data cannot be found"}');
                                callback('{"notify":true,"notify_msg":"Requested data cannot be found","notify_time":"5"}');
                            } else if(xhr.status===500) {
                                //callback('{"error":200,"error_msg":"Requested data encounter server error"}');
                                callback('{"notify":true,"notify_msg":"Requested data encounter server error","notify_time":"5"}');
                            } else {
                                //callback('{"error":42,"error_msg":"Connection to server is unavailable"}');
                                callback('{"notify":true,"notify_msg":"Connection to server is unavailable","notify_time":"5"}');
                            }
                        }
                    }
                };
                data = cineBoxFnObjToPost(data,'ajax');
                xhr.send(data);
            }
        };

// NAVIGATION SECTION -----------------------------------------------------
        /*
        -----------------------
        CUSTOMER SIDE LIBRARY
        -----------------------
        this.nav = {

            navCheck: function(type){

                if (navigator.geolocation){
                    if(type==0){
                        navigator.geolocation.getCurrentPosition(this.navGetList);
                    }
                    if(type==1)
                        navigator.geolocation.getCurrentPosition(this.navShowPos);
                    return true;
                }
                return false;
            },

            navShowPos: function(pos){
                var url;
                console.log('LATITUDE: '+ pos.coords.latitude);
                console.log('LONGITUDE: ' + pos.coords.longitude);
                if(url = $cineFnMainObjects.queryBase){
                    url += '?query='+$cineFnMainObjects.methodNavi;
                    var cine = new cineBox();
                    //40.577505, -73.968848
                    var lat = pos.coords.latitude;
                    var lng = pos.coords.longitude;
                    //40.853164, -73.968934
                    //var lat = 40.577505;
                    //var lng = -73.968848;
                    //var lat = 40.853164;
                    //var lng = -73.968934;
                    cine.ajax('post',url,{'lat':lat,'long':lng},function(msg){
                        var cine = new cineBox();
                        cine.nav.navPopForm(msg);
                    });
                }
            },
            navPopForm: function(data){
                if(fid = document.getElementById('cine_fn_form_search')){

                    fld = document.getElementById('cine_fn_loc_search');
                    var newel = document.createElement('p');
                    var r = JSON.parse(data);
                    r=cineBoxParseGeoQuery(r);

                    if(r){

                        var loc={subcity:false,city:false};
                        subrb= r.sublocality_level_1;
                        lclty= r.locality;
                        adara= r.administrative_area_level_1;

                        if(subrb){
                            loc['subcity']=subrb;
                            if(lclty!=adara){
                                loc['city']=adara;
                            } else {
                                loc['city']=lclty;
                            }
                        } else {
                            loc['subcity']= lclty;
                            loc['city']= adara;
                        }

                        fld.value=cineBoxObjToCommaString(loc);
                        newel.innerHTML = '<div style="display:inline-block; width:320px; text-align:left;">CITY:'+loc.city+' DISTRICT '+loc.subcity+'</div>';
                        fid.parentNode.insertBefore(newel,fid.nextSibling);
                    }

                }
            },
            navGetList: function(pos){
                var url;
                try {
                    if (url = $cineFnMainObjects.queryBase) {
                        url += '?query=' + $cineFnMainObjects.queryResto + '&type=' + $cineFnMainObjects.methodList;
                        var cine = new cineBox();
                        var lat = pos.coords.latitude;
                        var lng = pos.coords.longitude;
                        cine.ajax('post', url, {'lat': lat, 'long': lng, 'user': 0}, function (msg) {
                            cineBoxFnObjRestoList(msg, pos);
                        });
                    }
                } catch (err){
                    alert(err.message);
                }

            }
        };

        this.resourse = {
            // inline = display inline
            // name = name of the button
            // submit = define type submit
            // close = object linked to manipultaion for close
            // func = linked function to execute
            // method = CLICK / MOUSEDOWN etc
            // id = if not defined, using random one
            button: function(inline,name,submit,close,func,method,id,viewobj,defer,deferfunc){
                var b,tid;
                var c = new cineBox();
                if(inline){
                    b = document.createElement($cineFnMainObjects.domButtonForm);
                    b.className = $cineFnMainObjects.mbtnClass +' inline';
                    b.setAttribute('cinerootobject',viewobj);

                    if(id !== 'undefined' || id.length <1 ){
                        tid = new Date().getTime();
                        b.setAttribute('id','cine_color_btn'+tid);
                    }

                    if(submit)
                        b.setAttribute('type','submit');
                    else
                        b.setAttribute('type','button');

                    b.setAttribute('value',name);

                    if(typeof func === 'function')
                    {
                        if(method !== 'undefined' && method.length>0){
                            if(!close){b.addEventListener(method,function(e){func(e,defer,deferfunc);});}
                            else {b.addEventListener(method,function(e){func(e,defer,deferfunc); c.modal.process(close);});}
                        }
                        else{
                            if(!close)b.addEventListener('click',function(e){func(e,defer,deferfunc);});
                            else b.addEventListener('click',function(e){func(e,defer,deferfunc); c.modal.process(close);});
                        }
                    } else
                        b.addEventListener('click',function(e){c.modal.close(close);});
                }
                return b;
            }
        };
        */
        this.rexec = {

            get: function(e){
                var url; var obj = e.target; e.preventDefault();
                var rtp=obj.getAttribute('cinereqtype');
                var req=obj.getAttribute('cineaction');
                var qry=obj.getAttribute('cinequery');
                var bck=obj.getAttribute('cineback');
                var bnm=obj.getAttribute('cinebtnname');
                var evt=obj.getAttribute('cineevent');
                var usr=obj.getAttribute('cineuser');
                var idt=obj.getAttribute('cineident');
                var frm=obj.getAttribute('cineform');
                var ttl=obj.getAttribute('cinereqtitle');
                var spc=obj.getAttribute('cinespecial');

                var vars = {cinereqtype:rtp,cineaction:req,cinequery:qry,cineback:bck,cinebtnname:bnm,
                    cineevent:evt,cineuser:usr,cineident:idt,cineform:frm,cinereqtitle:ttl,cinespecial:spc};

                if(url = cineBoxFnObjBase()){
                    url += 'srv?query='+req+'&type='+qry;
                    var cine = new cineBox();
                    cine.ajax('post',url,{user:usr,ident:idt},function(msg){
                        cineBoxFnReqDataProc(msg,usr,rtp,vars,true);
                    });
                }
                return false;
            },

            getManual: function(e,attr){

                var o=cineBoxFnDomObject(e);
                var a=false;
                e=o.event;
                var obj=o.object;

                if(e){
                    e.preventDefault();
                    a=true;
                }

                if(a && obj!=null && cineBoxFnReturnClass(obj.className,'disabled')) return false;
                cineBlockButton(obj);

                var url,inst;
                var rtp,usr,key,idt,req,qry,evt,bnm,bck,ttl,spc,attl,rep,bkfn,adat;

                //var evtd=false;
                //var data=false;
                //var ext=false;
                //var win=true;

                /*if(typeof attr==='undefined'){

                    rtp=obj.getAttribute('cinereqtype');
                    usr=obj.getAttribute('cineuser');
                    idt=obj.getAttribute('cineident');
                    req=obj.getAttribute('cineaction');
                    qry=obj.getAttribute('cinequery');
                    evt=obj.getAttribute('cineevent');
                    bnm=obj.getAttribute('cinebtnname');
                    ttl=obj.getAttribute('cinereqtitle');
                    spc=obj.getAttribute('cinespecial');

                } else { */

                var win =(attr.hasOwnProperty('reqWindow'))?attr.reqWindow:true;
                var data=(attr.hasOwnProperty('reqData'))?attr.reqData:false;
                var evtd=(attr.hasOwnProperty('reqEventData'))?attr.reqEventData:false;
                var ext =(attr.hasOwnProperty('reqExt'))?attr.reqExt:false;

                    rtp=attr.reqType;
                    idt=attr.reqIdent;
                    req=attr.reqAction;
                    qry=attr.reqQuery;
                    evt=attr.reqEvent;
                    ttl=attr.reqTitle;
                    bnm=attr.reqBtnName;
                    spc=attr.reqSpecial;
                    bck=attr.reqBack;

                    inst=(attr.hasOwnProperty('reqInstance'))?attr.reqInstance:false;
                    bkfn=(attr.hasOwnProperty('reqBackBar'))?attr.reqBackBar:false;
                    usr =(attr.hasOwnProperty('reqUser'))?attr.reqUser:0;
                    key =(attr.hasOwnProperty('reqUserKey'))?attr.reqUserKey:false;
                    attl=(attr.hasOwnProperty('reqAppTitle'))?attr.reqAppTitle:false;

                    adat=(attr.hasOwnProperty('reqAddData'))?attr.reqAddData:false;
                    rep =(attr.hasOwnProperty('reqRepeat'))?attr.reqRepeat:false;

                //}
                if(inst){
                    if(inst=='stat')
                        url=$cineFnMainObjects.queryBase+$cineFnMainObjects.queryStat;
                    else if(inst=='ext')
                        url=$cineFnMainObjects.queryBase+$cineFnMainObjects.queryExt;
                    else
                        url=$cineFnMainObjects.queryBase+$cineFnMainObjects.queryApi;
                } else {
                    url=$cineFnMainObjects.queryBase+$cineFnMainObjects.queryApi;
                }

                bnm = (bnm=='false'||!bnm)?false:bnm;
                var ti=new Date(); ti=ti.getDate()+''+ti.getHours()+''+ti.getMinutes()+''+ti.getSeconds()+Math.round((999)*Math.random());

                var vars = {cinereqtype:rtp,cineaction:req,cinequery:qry,cineevent:evt,cineevdata:evtd,cineapptitle:attl,cineform:'',cineuserkey:key,cinebackbar:bkfn,
                    cineuser:usr,cineident:idt,cinereqtitle:ttl,cinerandom:ti,cinespecial:spc,cinewindow:win, cinebtnname:bnm,cineback:bck,cinerepeat:rep};

                var srvr = {user:usr,ident:idt,data:data,ext:ext,random:ti,adata:adat};
                if(spc) srvr['special']=spc;

                if(key)
                {
                    if(window.localStorage!=='undefined'){
                        srvr['user']=window.localStorage.getItem('user_id');
                        srvr['userkey']=window.localStorage.getItem('user_key');
                    }
                }

                //if(url = $cineFnMainObjects.queryBase){
                    url += '?query='+req+'&type='+qry;
                    var cine = new cineBox();
                    cine.ajax('post',url,srvr,function(msg){
                        cineBoxFnReqDataProc(msg,usr,rtp,vars,true);
                    });
                //}
                return false;

            }

            /*
            route: function(e,manual,rootobj,rtype,require,query,ext,defer,deferfunc,ident,attr){

                if(e!=false){
                    e.preventDefault();
                }

                var url,obj,data,rtp,req,qry,usr,dfn,cls,key,bkfn=false;

                // MOVE TO EXECUTE BY OBJECT
                if(typeof attr === 'undefined'){
                    if(typeof manual!=='undefined'){

                        if(rootobj!=false)
                            obj=document.getElementById(rootobj);
                        else
                            obj=false;

                        rtp=rtype;
                        req=require;
                        qry=query;
                        usr=0;
                        cls=true;

                    } else {
                        // AUTO EXECUTION
                        obj = e.target;
                        rtp=obj.getAttribute('cinereqtype');
                        req=obj.getAttribute('cineaction');
                        qry=obj.getAttribute('cinequery');
                        usr=obj.getAttribute('cineuser');
                        dfn=obj.getAttribute('cineevent');
                        cls=obj.getAttribute('cineclose');
                        key=obj.getAttribute('cineuserkey');
                        bkfn=obj.getAttribute('cinebackbar');

                        key = (key=='true');
                        cls = (cls=='true');
                        window.settings={usercineevt:dfn};
                        deferfunc = window.settings.usercineevt;
                        if(typeof deferfunc === "function")
                            defer=true;
                        obj = e.target.parentNode;

                    }
                } else {
                    // OBJECT EXECUTION

                    obj=attr.reqObject; // e.target --> rootobj
                    ext=attr.reqExtData;// external parameter
                    data=attr.reqData;  // data parameter
                    rtp=attr.reqType;   // cinereqtype
                    req=attr.reqAction; // cinereqaction
                    qry=attr.reqQuery;  // cinequery
                    key=(attr.hasOwnProperty('reqUserKey'))?attr.reqUserKey:false;
                    usr=0;
                    dfn=attr.execFn;    // cineevent
                    cls=(attr.hasOwnProperty('execClose'))?attr.execClose:false;

                    window.settings={usercineevt:dfn};
                    deferfunc = window.settings.usercineevt;
                    if(typeof deferfunc === "function")
                        defer=true;
                }

                if(typeof ident ==="undefined")
                    ident = 0;

                if(obj && obj!==null)
                    var ret = cineBoxFnDomTraverse(obj,'*:cinedata');

                else
                    ret=data;

                if(ret.length>0)
                    ret = cineBoxFnEntityToJSON(ret);

                if(typeof ext !== 'undefined' && ext.length<1)
                    ext = '';

                if(!ret)
                    ret='';

                if(rtp=='view' || rtp=='popup'){

                    var srvr = {user:usr,ident:ident,data:ret,ext:ext};

                    if(key){
                        if(window.localStorage!=='undefined'){
                            srvr['user']=window.localStorage.getItem('user_id');
                            srvr['userkey']=window.localStorage.getItem('user_key');
                        }
                    }

                    if(url = $cineFnMainObjects.queryBase){
                        url += '?query='+req+'&type='+qry;
                        var cine = new cineBox();
                        cine.ajax('post',url,srvr,function(msg){

                            cineBoxFnReqDataUpdate(msg);

                            if(defer){

                                if(typeof deferfunc === 'function'){
                                    deferfunc(msg);
                                }
                                if(cls)
                                    obj.parentNode.removeChild(obj);

                            }

                            if(bkfn){
                                if(typeof bkfn!=='object'){
                                    var prev=document.getElementById(bkfn);
                                    if(prev!=null)
                                        cineBoxFnRestoMenuBack(false,true,prev,false);
                                } else
                                    cineBoxFnRestoMenuBack(false,true,bkfn,false);
                            }

                        });
                    }
                }

                return false;
            }

            /* DEPRECATED
            --------------------

            form: function(type,obj,data){
                var url;
                if(typeof data === 'undefinded')
                    data = '';
                var req=$cineFnMainObjects.methodForm;
                var srvr={ext:data,user:0,userkey:0};
                if(window.localStorage!=='undefined'){
                    srvr['user']=window.localStorage.getItem('user_id');
                    srvr['userkey']=window.localStorage.getItem('user_key');
                }
                if(url = $cineFnMainObjects.queryBase){
                    url += '?query='+req+'&type='+type;
                    var cine = new cineBox();
                    cine.ajax('post',url,srvr,function(msg){
                        cineBoxFnAddObjToObj(obj,msg);
                    });
                }
                return false;
            }

            */
        };

    this.modal = {

        windowid:false,

        ident: null,
        special:null,
        requser: null,
        requserkey: false,
        reqtype: null,
        attribs: null,
        query: null,
        btnname: 'Button',
        frmtitle: 'Window',
        apptitle: false,
        centered: false,
        backbar: false,

        submitfunc: false,
        secondfunc: false,
        thirdfunc: false,
        // data = HTML formatted data
        // bfrm = Frame where to set button
        // evnt = Save button event
        // type = Boolean type (desktop format / mobile format)
        // centr = Set the text-align center property
        set: function(data,bfrm,usr,rtyp,attr,type,centr){

            type = typeof type !== 'undefined' ? type : false;

            this.ident = attr.cineident;
            this.centered = centr;
            this.requser = usr;
            this.reqtype = rtyp;
            this.attribs = attr;
            this.query = attr.cinequery;
            this.btnname = attr.cinebtnname;
            this.frmtitle = attr.cinereqtitle;
            this.apptitle = attr.cineapptitle;
            this.special = attr.cinespecial;
            this.requserkey = attr.cineuserkey;
            this.backbar = attr.cinebackbar;

            if(cineObjectIndex(attr,'cinerandom')){
                this.windowid=attr.cinerandom;
            }

            if(type){
                return this.show(type,data,bfrm);
            }

            return false;
        },
        // type = there goes variants of button
        // obj = where to put
        // exec = attributes to place
        // close = object which terminate from DOM
        button: function(type,obj,exec,close){

            var b,p;
            var c = new cineBox();

            if(type==0){
                b = document.createElement($cineFnMainObjects.domButton);
                b.className = $cineFnMainObjects.btnClass;
            }

            if(type==1){

                p=close.id;
                b = document.createElement($cineFnMainObjects.domButtonForm);
                b.className = 'cine_fn_color_btn';
                b.setAttribute('type','submit');
                b.setAttribute('value',this.btnname);
                b.setAttribute('cineparent',p);
                this.attribs.cinequery=this.attribs.cineback;
                var u=this.attribs;
                for(var k in u){
                    if(u.hasOwnProperty(k))
                        b.setAttribute(k,u[k]);
                }

                if(obj.id.length > 0){
                    b.setAttribute('id','dsadsadsa');
                } else {
                    cn = obj.className;
                    b.setAttribute('id',cn);
                }
                obj.appendChild(b);
                b.addEventListener('click',function(e){c.rexec.route(e); c.modal.process(close);});
            }

        },
        // pos = type from set prefunction
        // cont = data from set prefunction
        show: function(pos,cont,bfrm){

            var m,i,f,mt,ma,mw,mh,ms,dh;

            dh=window.innerHeight;
            m = document.createElement($cineFnMainObjects.domLayer);
            m.className = $cineFnMainObjects.modalClass+ ' cinemodal cinevisible';

            if(this.backbar){
                var bar;
                if(this.frmtitle.length>0){
                    bar=cineBoxFnRestoMenuBack(false,false,this.backbar,m,this.frmtitle);
                } else
                    bar=cineBoxFnRestoMenuBack(false,false,this.backbar,m);
                m.appendChild(bar);
                this.attribs.cinebackbar=bar.getAttribute('cineident');
            }
            if(this.apptitle){
                ma = document.getElementById('cine_fn_ctitle');
                ma.innerHTML=this.apptitle;
                m.setAttribute('cinetitle',this.apptitle);
            }

            f=cont;
            i=this.windowid;

            m.style.cssText = 'min-width:'+$cineFnMainObjects.modalDefW+'px;min-height:'+$cineFnMainObjects.modalDefH+'px;';
            m.style.left = -1000;
            m.style.top = -1000;
            m.style.display = 'block';
            m.setAttribute('cineident',this.ident);
            m.setAttribute('cinespecial',this.special);

            if(!this.windowid){
                i=new Date(); i=i.getDay()+''+i.getHours()+''+i.getMinutes()+''+i.getSeconds()+''+ i.getMilliseconds();
                m.setAttribute('id','cinemodal' + i);
                m.setAttribute('modalid',i);
            } else {
                m.setAttribute('id','cinemodal'+i);
                m.setAttribute('modalid',i);
            }

            if(this.btnname)
                this.button(1,bfrm,'',m);

            //m.appendChild(mt);
            f.setAttribute('id','cinecont'+i);
            m.appendChild(f);
            document.body.appendChild(m);
            ms = 'top:76px;';

            if(this.centered)
                ms += 'text-align:center;';

            m.style.cssText = 'min-width:'+$cineFnMainObjects.modalDefW+'px;min-height:'+$cineFnMainObjects.modalDefH+'px;'+ms;

            md = getComputedStyle(m,null).getPropertyValue('display');

            if(md == 'none'){
                m.style.display = 'block';
            }

            cineFnCurrentWindow=m;
            cineFnWindows.push(i);

            return m;
        },
        // initiator = event.target
        // title = title of subframe
        // content = passed form
        // endfunc = function to pass in submit
        // btype = 1 submit [2 cancel]
        // bname = buttonname1,buttonname2
        // defer = close layer by other processing
        //initiator,title,content,type,btype,bname,defer,deferfunc,submitfunc,secondfunc,thirdfunc
        /*
        submodal: function(attr){
            var c = new cineBox();
            var obj,mdl,tmp,stl,zdx,fre;
            var sobj,sid,scl,btn,btn1,btn2,btn3,bhld,wdt;

            var i,t,ct,s,b,n,d,f,sm,sn,td,w,h,cr,cu,ias;
            if(typeof attr === 'object'){
                i=attr.init;
                t=attr.title;
                ct=attr.content;
                s=attr.type;
                b=attr.buttons;
                n=attr.btnname;
                d=attr.isdefer;
                f=attr.deferfn;
                sm=attr.submitfn;
                sn=attr.secondfn;
                td=attr.thirdfn;
                w=attr.width;
                h=attr.height;
                cr=attr.center;
                cu=attr.custom;
                ias=attr.initAsForm;
            } else
                return false;

            obj=document.getElementById(i);
            mdl = cineBoxFnFindModal(obj);

            if(mdl){

                var wht=window.innerHeight;
                wht=wht-76;

                stl = mdl.style.cssText;
                zdx = mdl.style.zIndex;
                if(zdx === 'NaN'){
                    zdx=1000;
                } else zdx=parseInt(zdx);
                zdx += 1;

                sid = mdl.id+'sm';
                sobj = document.createElement('div');
                sobj.className = $cineFnMainObjects.modalClass+' cinesubmodal';
                sobj.setAttribute('id',sid);
                sobj.setAttribute('modalid',mdl.id);

                tmp=document.createElement('h3');
                tmp.innerHTML = t;

                if(s==1){
                    sobj.style.backgroundColor = '#333333';
                    sobj.style.minHeight = mdl.offsetHeight+'px';
                    sobj.style.zIndex = zdx;
                    tmp.className='white';
                }
                if(s==2){
                    sobj.style.backgroundColor = '#F8F8F8';
                    sobj.style.minHeight = mdl.offsetHeight+'px';
                    sobj.style.zIndex = zdx;
                    tmp.className='black';
                }

                sobj.appendChild(tmp);

                // Form injection
                if(ias){
                    var frm=document.createElement('form');
                    frm.setAttribute('id','submodalform');
                    frm.setAttribute('modalid',mdl.id);
                    sobj.appendChild(frm);
                }

                if(typeof ct==='object'){
                    tmp.appendChild(ct);
                } else {
                    tmp.insertAdjacentHTML('afterend',ct);
                }

                document.body.appendChild(sobj);

                sobj.style.cssText = stl;
                fre=0;

                if(w!='auto'){
                    sobj.style.minWidth=w;
                    sobj.style.width=w;
                    fre++;
                }

                if(h!='auto'){
                    sobj.style.height=h;
                    fre++;
                }

                sobj.style.minHeight=wht+'px';

                if(cr)
                    sobj.style.marginLeft=-(sobj.offsetWidth/2)+'px';

                if(typeof cu==="object"){
                    for(var k in cu){
                        if(cu.hasOwnProperty(k)){
                            sobj.setAttribute(k,cu[k]);
                        }
                    }
                }

                var bobj;
                if(ias){
                    bobj=frm;
                } else {
                    bobj=sobj;
                }
                bhld = document.createElement('p');
                btn=this.btnnames(n);

                if(b==2)
                {
                    btn1 = c.resourse.button(true,btn[0],true,false,sm,'','',sobj.id,d,f);
                    btn2 = c.resourse.button(true,btn[1],false,sobj,sn);

                    bhld.appendChild(btn1);
                    bhld.appendChild(btn2);
                    bobj.appendChild(bhld);

                    wdt=[btn1,btn2];
                    this.btnwidth(wdt);
                }
                if(b==3)
                {
                    btn1 = c.resourse.button(true,btn[0],true,false,sm,'','',sobj.id,d,f);
                    btn2 = c.resourse.button(true,btn[1],false,sobj,sn,'','',sobj.id,d,f);
                    btn3 = c.resourse.button(true,btn[2],false,false,td,'','',sobj.id,d,f);

                    bhld.appendChild(btn1);
                    bhld.appendChild(btn2);
                    bhld.appendChild(btn3);
                    bobj.appendChild(bhld);

                    wdt=[btn1,btn2,btn3];
                    this.btnwidth(wdt);
                }
                if(ias){
                    return frm;
                }
                return sobj;
            }
            return false;
        },

        /*headers: function(modal,chain){
            var o;
        },

        btnnames: function(names){
            var l,i,str,def; str=names; names=names.split(','); def=['Ok','Cancel','Delete'];
            l=names.length;
            if(l>0){for(i=0;i<l;i++){def[i]=names[i];}
            } else {if(str.length>0)def[0]=str;}
            return def;
        },

        btnwidth: function(elems){
            var l,i,wdt,max; l=elems.length; max=1;
            for(i=0;i<l;i++){
                wdt=elems[i].clientWidth;if(wdt>max)max=wdt;
            }
            for(i=0;i<l;i++){
                elems[i].style.width = max+'px';
            }
        },
        */
        close: function(e){
            if(typeof e !== 'undefined')
                e.parentNode.removeChild(e);
        },

        process: function(object){
            event.preventDefault();
            this.close(object);
        },

        align: function(o){
            cineBoxFnModalAfterLoad(o);
        },

        destroyThis: function(o){
            cineWinCtrlRemWin(false,o);
        },

        destroyCurrent: function(){
            cineWinCtrlRemWin(true);
        }


    };

        /*
    this.action = {


        cardprocess: function(e,defer,deferfunc){
            cineBoxFnloadScript($cineFnMainObjects.bTreeLink,e,cineBoxFnProcessUserCard,defer,deferfunc);
        }


    };
         */

    this.helper = {

        domTraverse: function(obj,param){
            return cineBoxFnDomTraverse(obj,param);
        },
        domToJson: function(domArray){
            return cineBoxFnEntityToJSON(domArray);
        },
        findClass: function(s,c){
            return cineBoxFnReturnClass(s,c);
        },
        arrayClean: function(a){
            return cineBoxFnArrayFilled(a);
        },
        frameLabeled: function(o){
            return cineBoxLabelFrame(o);
        },
        objectIndex: function(o,v){
            return cineObjectIndex(o,v);
        }

    };

    this.resto = {
        getMenu: function(e,d){
            cineBoxFnRestoOrderMenu(e,d);
        },
        renderMenu: function(d,l,g,sg,i){
            return cineBoxFnRestoRendrMenu(d,l,g,sg,i);
        }
    };

    this.menu = {
        sideSet: function(e){
            cineBoxFnRestoMenuSideSet(e);
        }
    };

    this.order = {
        coItems: function(o,i,k,f,d,s){
            return cineBoxFnRestoOrderItemList(o,i,k,f,d,s);
        },
        coSummary: function(p,t,u){
            return cineBoxFnRestoOrderItemSumm(p,t,u);
        },
        price: function(type){
            return this.retNumber(cOrderPrice.priceCash,type);
        },
        tax: function(type){
            return this.retNumber(cOrderPrice.taxCash,type);
        },
        tip: function(type){
            return this.retNumber(cOrderPrice.tipCash,type);
        },
        total: function(type){
            return this.retNumber(cOrderPrice.totalCash,type);
        },
        countPrices: function(p,t,u){
            cOrderPriceCount(p,t,u);
        },
        setNumbers: function(price,tax,tips,total,taxP,tipP){
            cOrderPrice.priceCash=price;
            cOrderPrice.taxCash=tax;
            cOrderPrice.tipCash=tips;
            cOrderPrice.totalCash=total;
            cOrderPrice.tipPercent=tipP;
            cOrderPrice.taxPercent=taxP;
        },
        retNumber: function(num,type){
            if(typeof type!=='undefined'){
                if(type=='num'){
                    return num;
                } else if (type=='str'){
                    return num.toFixed(2);
                }
            } else {
                return num;
            }
        }
    }
};
    cineBox.prototype.ajax = function(type,url,data,event){
        if(type=='post'){
            var ajo = this.ajaxfunc.initObj();
            if(ajo){
                return this.ajaxfunc.postData(ajo,url,data,event);
            }
        }
    };

    // AJAX HELPER SERIALIZE TO URL // JS Object notation {"key":value,...} to key=value&....
    function cineBoxFnObjToPost(data,qtype){
        var r = [];
        for (var k in data){
            r.push(encodeURIComponent(k) + '=' + encodeURIComponent(data[k]));
        }r.push('query_type='+qtype); return r.join('&');
    }
    // BASE APPLICATION URL RETRIEVE FROM PREDEFINED OBJECT IN TEMPLATE
    function cineBoxFnObjBase(){var obj;
        if(obj=document.getElementById('cine_fn_base_url_object')){
            return obj.value;
        } return false;
    }
    // ANIMATION CONTAINERS PROPER VIEW CONTROLLER
    // DEPRECATED
    /*
    function cineBoxFnAnimResize(){
        var obj,chld,pnth,pntw,ht,wd,hn,atr,cc;
        var r=1;
        if(obj=document.getElementById($cineFnMainObjects.pageAnims)){
            var a=obj.value.split(',');
            for (var k in a){
                obj=document.getElementById(a[k]);
                chld=obj.childNodes;
                for(var c in chld){
                    cc = chld[c];
                    if(cc.nodeType==1){
                        pnth=obj.clientHeight;
                        pntw=obj.clientWidth;
                        ht =cc.clientHeight;
                        wd =cineBoxFnCoordPxRem(window.getComputedStyle(cc,null).getPropertyValue('width'));
                        if(atr = chld[c].getAttribute('ratio'))
                            r=atr;
                        else {
                            r=(wd/ht).toFixed(2);
                            ht=pnth;
                            cc.setAttribute('ratio',r);
                        }
                        hn = pntw/r;
                        if(hn>pnth)
                            atr='margin-top:'+((pnth-hn)/2)+'px;margin-left:0px;';
                        else {
                            wd=ht*r;
                            w=pntw-wd;
                            atr='width:'+wd+'px;margin-top:0px;margin-left:'+ ((w<0) ? (w/2) : w) +'px;';
                        }
                        cc.style.cssText = 'min-height:'+pnth+'px;'+atr;
                    }
                }
            }
        }
    }
    */
    // USER NAVIGATION PANELS SIZE AND PLACE
    //function cineBoxFnUserNavView(){

    //}
    // USER NAVIGATION PANELS SIZE AND PLACE
    // [json string] data = JSON Format string
    // [int] usr = User id number
    // [string name] rtp = Request type [view],[popup]
    // [string name] attr = JS OBJECT with variables ATTRIBUTES etc.
    // [bool] centro = Center form in frame with sublayer
    function cineBoxFnReqDataProc(data,usr,rtp,vars,centro){

        /* ---------------- RENDERING DESCRIPTION ---------------------
         NONE / title / type / void / void
         'id'=>array('ident','none',0,0),
         INPUT / title / type / length / control_function_format
         'login'=>array('Login','input',32,0),
         REQUEST / title / type / what_to_req / void
         'timezone'=>array('Time Zone','request','timezone',0),
         INPUT / title / type / length / control_function_format
         'phone'=>array('Phone number','input',10,'phone'),
         QUERY INPUT / title / type / length / what_to_req / query_name
         'address_country'=>array('Country','qinput',32,'geo','country')
         */

        var obj = null;
        var prx = $cineFnMainObjects.reqPrefix;
        var elm,etl,cur,i,cval,cntr;

        var error=false;
        var ernum=0;

        if(data[0]=='{'){
            data = JSON.parse(data);
            if (!data.result && data.error > 0) {
                error=true;
                ernum=data.error;
            }
            if(data.notify){
                if('notify_time' in data){
                    cineNotify({message:data.notify_msg,destroy:data.notify_time});
                } else {
                    cineNotify({message:data.notify_msg});
                }
            }

        }

        var cnt = document.createElement($cineFnMainObjects.domForm);
        cnt.className = 'cine_fn_form_default';
        var fmt = true;
        cnt.style.border = 'none';
        cnt.style.width = 'auto';
        cnt.style.padding = '4%';
        cnt.style.textAlign = 'center';

        var c = new cineBox();

        if(rtp=='view'){
            var dms = [];
            frm=vars.cineform;
            if(frm.length > 1 && document.getElementById(frm) !== 'undefined')
                fmt=false;

            var dr=data['result'];

            for (var k in dr){
                elm=null;
                cval='';
                cur=dr[k];
                i=cur[1];
                if(dr.hasOwnProperty(k)){
                    if (cineObjectIndex(dms,k))
                        cval = dms[k];
                    if (k == 'dataoutput') dms = dr[k];

                    if (i == 'input') {
                        etl = cineBoxFnDomCreateEl('label', '', '', '', cur[0], false);
                        if (fmt)
                            elm = cineBoxFnDomCreateEl(i, prx + k, 'cinedata', 'block', cval, false);
                        else
                            elm = cineBoxFnDomCreateEl(i, prx + k, 'cinedata', '', cval, false);
                        elm.setAttribute('maxlength', cur[2]);
                    }
                    if (i == 'select') {
                        etl = cineBoxFnDomCreateEl('label', '', '', '', cur[0], false);
                        elm = cineBoxFnDomCreateEl(i, prx + k, 'cinedata', 'block', cur[2], true, cval);
                    }
                    if (i == 'qinput') {
                        etl = cineBoxFnDomCreateEl('label', '', '', '', cur[0], false);
                        if (fmt)
                            elm = cineBoxFnDomCreateEl('input', prx + k, 'cinedata', 'block', cval, false);
                        else
                            elm = cineBoxFnDomCreateEl('input', prx + k, 'cinedata', '', cval, false);
                        elm.setAttribute('maxlength', cur[2]);
                    }
                    if (i == 'textarea') {
                        etl = cineBoxFnDomCreateEl('label', '', '', '', cur[0], false);
                        elm = cineBoxFnDomCreateEl(i, prx + k, 'cinedata', 'block', cval, false);
                    }
                    if (i == 'none') {
                        etl = cineBoxFnDomCreateEl('label', '', '', 'none', cur[0], false);
                        elm = cineBoxFnDomCreateEl('input', prx + k, 'cinedata', 'block', cval, false);
                        elm.setAttribute('type', 'hidden');
                    }

                    if (elm != null) {
                        cnt.appendChild(etl);
                        cnt.appendChild(elm);
                    }
                }
            }

            if(centro){
                cntr = document.createElement($cineFnMainObjects.domLayer);
                cnt.style.display = 'block';
                cntr.className = 'cine_fn_modal_centerer';
                cntr.appendChild(cnt);
                obj = c.modal.set(cntr,cnt,usr,rtp,vars,true,true);
            } else
                obj = c.modal.set(cnt,cnt,usr,rtp,vars,true);
        }
        /*
        if(rtp=='popup'){
            if(data['success'] == 'ok'){
                alert('Popup form executed');
            }
        }
        */
        if(rtp=='man'){

            if(typeof cineFnCurrentWindow==='object'){
                vars['cinebackbar']=cineFnCurrentWindow;
            }

            if(vars.cinewindow){
                if(centro){
                    cnt.style.display = 'block';
                    cntr = document.createElement($cineFnMainObjects.domLayer);
                    cntr.className = 'cine_fn_modal_centerer';
                    if(typeof data !== 'object')
                        cntr.innerHTML = data;
                    obj = c.modal.set(cntr,cnt,usr,rtp,vars,true,true);
                } else
                    obj = c.modal.set(cnt,cnt,usr,rtp,vars,true,false);
            }

        }

        if(error){

            if(ernum==1000){
                var lobj;
                if(obj!=null){
                    mid=obj.getAttribute('modalid');
                    cid=document.getElementById('cinecont'+mid);
                    lobj=cineBoxFnRequireLogin(obj,vars.cinerepeat);
                    cid.appendChild(lobj);
                    cineBoxFnModalAfterLoad(obj);
                } else {
                    //obj=document.getElementsByClassName('cineRoot');
                    var tm=cOpenTinyModal(false);
                    lobj=cineBoxFnRequireLogin(tm,vars.cinerepeat);
                    //obj[0].style.cssText='text-align:center';
                    tm.content.appendChild(lobj);
                }
                return false;
            } else {

                cineNotify({message:'Error: '+data['error']+' '+data['error_msg'],destroy:0});
                //alert('Error: '+data['error']+" "+data['error_msg']);
            }

        }

        // If object window not defined then we process cinespecial instead object
        // Purpose of reqWindow -> false, but you still need to kill or use defined window
        if(obj==null){
            obj=vars.cinespecial;
        }
        // Fire described function in field < cineEvent >
        var evt = vars.cineevent;

        if(typeof evt==='function'){
            var r;
            if(!error&&typeof vars.cinerepeat==='function'){
                r=vars.cinerepeat;
            }
            if(vars.cineevdata){
                evt(obj,data,r);
            }
            else{
                evt(obj,r);
            }
        }
        else
        {
            window.settings={uevt:evt};
            var ufn = window[settings.uevt];
            if(typeof ufn === "function"){
                if(vars.cineevdata){
                    ufn(obj,data);
                }
                else{
                    ufn(obj);
                }
            }
        }

        if(typeof obj==='object')
            cineBoxFnModalAfterLoad(obj);
    }
    function cineBoxFnRequireSignup(e,o,r,form){

        if(o.hasOwnProperty('content')){
            cc=o.content;
            cc.style.overflowY='scroll';
        } else {
            var cc=o.getAttribute('modalid');
            if(cc!=null)
                cc=document.getElementById('cinecont'+cc);
            else
                cc=o;
        }


        var uemail='cine_fn_email';
        var uname='cine_fn_username';
        var upass='cine_fn_password';
        var upassr='cine_fn_password2';
        var ufname='cine_fn_firstname';
        var ulname='cine_fn_lastname';

        var f=document.createElement('form');
        f.className='cine_fn_form_default';
        var h='';

        h+='<label>Email address</label>';
        h+='<input type="text" name="'+uemail+'" id="'+uemail+'" value="">';
        h+='<label>Unique Login</label>';
        h+='<input type="text" name="'+uname+'" id="'+uname+'" value="">';
        h+='<label>Password</label>';
        h+='<input type="password" name="'+upass+'" id="'+upass+'" value="">';
        h+='<label>Repeat Password</label>';
        h+='<input type="password" name="'+upassr+'" id="'+upassr+'" value="">';
        h+='<label>First Name</label>';
        h+='<input type="text" name="'+ufname+'" id="'+ufname+'" value="">';
        h+='<label>Lastname</label>';
        h+='<input type="text" name="'+ulname+'" id="'+ulname+'" value="">';

        //var id=form;

        f.innerHTML=h;
        f.style.marginTop='20px';
        f.style.marginBottom='20px';
        var snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=200+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Sign up';
        snb.setAttribute('id','cine_login_button');
        snb.addEventListener('click',function(e){cineBoxFnProcessRegister(e,this.func,this.creds,this.form)}
            .bind({func:r,creds:{email:uemail,name:uname,pass:upass,pass2:upassr,firstname:ufname,lastname:ulname},form:form}));
        f.appendChild(snb);

        snb=document.createElement('h4');
        snb.innerHTML='If you have already registered you can log in:';
        snb.style.width=200+'px';
        f.appendChild(snb);

        snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=200+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Login';
        snb.setAttribute('id','cine_signup_button');
        snb.addEventListener('click',function(){cineBoxFnRequireLogin(this.obj,this.ret,true)}.bind({obj:o,ret:r}));
        f.appendChild(snb);

        cineFnClearDOM(cc);
        cc.appendChild(f);

        if(!cineTOS){
            cineBoxSignupTOS(o);
        }

    }
    function cineBoxSignupTOS(e){
        var param = {
            reqType:'man',
            reqIdent:1,
            reqData:'',
            reqAction:'appinfo',
            reqQuery:'gettos',
            reqEvent: cineBoxShowTOS,
            reqEventData: true,
            reqWindow: false,
            reqTitle:'',
            reqAppTitle:"",
            reqBtnName:false,
            reqSpecial:false,
            reqExt:'api'
        };
        var c=new cineBox();
        c.rexec.getManual(e,param);
    }
    function cineBoxShowTOS(e,o){
        if(o.success){
            var tm=cOpenTinyModal({
                buttonShow:true,
                buttonName:'Accept',
                buttonEvent:function(){cineTOS=true;}
            });
            var div=document.createElement('div');
            div.className='touch-scroll';
            div.style.cssText='height:80%; width:100%; border-bottom:1px solid #CCCCCC; overflow-y:scroll;';
            div.innerHTML=o.result['content'];
            cont=tm.content;
            cont.appendChild(div);
        }
    }
    function cineBoxFnProcessRegister(e,r,n,f){

        e.preventDefault();

        var cur;
        var dat={};
        for(var k in n){
            cur=document.getElementById(n[k]);
            dat[k]=cur.value;
        }

        var c=new cineBox();
        var t=cineBoxFnDomObject(e.target);

        var param = {
            reqType:'man',
            reqIdent:0,
            reqData:JSON.stringify(dat),
            reqAction:'userauth',
            reqQuery:'makeuserregister',
            reqEvent: cineBoxFnAfterLogin,
            reqEventData: true,
            reqWindow: false,
            reqTitle:'',
            reqAppTitle:"",
            reqBtnName:'',
            reqSpecial:f,
            reqExt:'api',
            reqRepeat:r
        };

        c.rexec.getManual(t,param);
    }
    function cineBoxFnRequireLogin(e,r,t){

        var uname='cine_fn_username';
        var upass='cine_fn_password';

        var f=document.createElement('form');
        f.className='cine_fn_form_default';
        var h='';

        h+='<label>Login or email</label>';
        h+='<input type="text" name="cine_fn_username" id="'+uname+'" value="">';
        h+='<label>Password</label>';
        h+='<input type="password" name="cine_fn_password" id="'+upass+'" value="">';

        //var id=e.getAttribute('id');

        f.innerHTML=h;
        f.style.marginTop='40px';
        var snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=200+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Login';
        snb.setAttribute('id','cine_login_button');
        snb.addEventListener('click',function(e){cineBoxFnProcessLogin(e,this.func,this.uname,this.upass,this.form)}.bind({func:r,uname:uname,upass:upass,form:e}));
        f.appendChild(snb);

        snb=document.createElement('h4');
        snb.innerHTML='If you have not registered yet you can sign up:';
        snb.style.width=200+'px';
        f.appendChild(snb);

        snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.style.width=200+'px';
        snb.setAttribute('type','button');
        snb.innerHTML='Sign up';
        snb.setAttribute('id','cine_signup_button');
        snb.addEventListener('click',function(e){cineBoxFnRequireSignup(e,this.obj,this.ret,this.form)}.bind({obj:e,ret:r,form:e}));
        f.appendChild(snb);

        if(typeof t!=='undefined'){
            if(e.className=='cine_fn_modal_default cinemodal cinevisible'){
                var a=e.getAttribute('modalid');
                e=document.getElementById('cinecont'+a);
            } else {
                e=e.content;
            }
            cineFnClearDOM(e);
            e.appendChild(f);
        }
        else
            return f;
    }

    function cineBoxFnProcessLogin(e,r,n,p,f){

        e.preventDefault();

        var lg=document.getElementById(n);
        var pw=document.getElementById(p);

        var c=new cineBox();
        var t=cineBoxFnDomObject(e.target);

        var param = {
            reqType:'man',
            reqIdent:lg.value,
            reqData:pw.value,
            reqAction:'userauth',
            reqQuery:'makeuserlogin',
            reqEvent: cineBoxFnAfterLogin,
            reqEventData: true,
            reqWindow: false,
            reqTitle:'',
            reqAppTitle:"",
            reqBtnName:'',
            reqSpecial:f,
            reqExt:'api',
            reqRepeat:r
        };

        c.rexec.getManual(t,param);
    }

    function cineBoxFnAfterLogin(e,d,r){

        if(d.success){

            var res= d.result;
            if(window.localStorage!=='undefined'){
                window.localStorage.setItem('user_id',res['user_id']);
                window.localStorage.setItem('user_key',res['user_key']);
            }

            //e=document.getElementById(e);
            //e.innerHTML='';

            if(e!=null && e.nodeType==1){
                if(cineBoxFnReturnClass(e.className,'cinemodal')){
                    cineWinCtrlRemWin(true);
                } else {
                    e.innerHTML='';
                }
            } else {
                e.under.parentNode.removeChild(e.under);
                e.modal.parentNode.removeChild(e.modal);
            }
            r();

        } else {


        }

    }

    // DATA FIELDS UPDATE AFTER REXEC ROUTE REQUEST DONE
    function cineBoxFnReqDataUpdate(data){
        var obj,indx;
        var prefix=$cineFnMainObjects.infoPrefix;
        if(data.length>0 && data[0]=='{'){
            data = JSON.parse(data);
            if(cineObjectIndex(data,'success')){
                data=data['result'];
                indx=parseInt(data['id']);
                if(indx==0)indx='';
                for(var k in data){
                    obj=document.getElementById(prefix+k+indx);
                    if(obj!=null){
                        obj.innerHTML=data[k];
                    }
                }
            }
            else{
                for(var k in data){
                    obj=document.getElementById(prefix+k);
                    if(obj!=null){
                        obj.innerHTML=data[k];
                    }
                }
            }
        }
    }
    // COMPUTED STYLE TO INTEGER
    function cineBoxFnCoordPxRem(t){var r=/[(px+)(%+)]+/g;return parseInt(t.replace(r,''));}
    // OBJECT FAST INDEX SEARCH PROTOTYPES
    Array.prototype.indexElm = function(val){var l=this.length;for(var i=0;i<l;i++){if(this[i]===val)return{result:true,pos:i}}return false;};
    // Object.prototype.indexElm = function(val){for(var i in this){if(i===val)return true}return false;};
    // Rewrite object proto to function in case of jquery
    function cineObjectIndex(obj,val){for(var i in obj){if(i===val)return true}return false;};
    // OBJECT ARRAY CLEAN TO ONLY FILLED CELLS and RETURN ARRAY / LENGTH / STRING COMMA SEPARATED
    function cineBoxFnArrayFilled(a){var l=a.length; var e=l-1; var n=[]; var s=0; var v=''; console.log(a);
        for(var i=0;i<l;i++){
            if(a.hasOwnProperty(i)){
                if(a[i].length>0){
                    n[i]=a[i];
                    s++;
                    if(i<l-1)
                        v+=a[i]+',';
                    else
                        v+=a[i];
                }
            }
        }
        return {array:n,length:s,string:v};
    }
    // RETURN CLASS NAME OF FALSE
    function cineBoxFnReturnClass(s,c){
        var r=false;
        if(typeof(s)=='string'){s=s.split(' ');
            for(var i=0;i<s.length;i++){if(s[i]==c){r=s[i];}}
        }return r;}
    // DOM ELEMENTS (which can have value) TO JSON STRING
    function cineBoxFnEntityToJSON(obj){
        var i,l,z,r; r=false;
        if(Object.prototype.toString.call(obj) == '[object Array]'){l=obj.length;z=l-1;r='';
            for(i=0;i<l;i++){
                if(i!=z)
                    r+='"'+obj[i].id+'":"'+obj[i].value+'",';
                else
                    r+='"'+obj[i].id+'":"'+obj[i].value+'"';
            }
            r='{'+r+'}';
        }
        return r;
    }
    // APPEND SOME DATA IN THE BEGINNING OF DEFINED OBJECT
    /*function cineBoxFnAddObjToObj(o,d){var c;if(typeof o==='string')o=document.getElementById(o);
        c=o.childNodes;if(c.length>0){c=c[0];if(typeof d==='object')c.parentNode.insertBefore(d,c);
        else{c.insertAdjacentHTML('afterend',d);}return true}return false;
    }*/
    // FIND MODAL PARENT
    function cineBoxFnFindModal(obj){
        var tobj,cls='cinemodal';
        if(typeof obj === 'string'){
            obj = document.getElementById(obj);
        }
        if(typeof obj === 'object'){
            tobj=obj;
            do {
                if(cineBoxFnReturnClass(tobj.className,cls)){
                    return tobj;
                } else {
                    tobj=tobj.parentNode;
                }
            } while(tobj.tagName !== 'BODY');
        }
        return false;
    }
    // PROCESS USER CARD WITH SUBMIT OR NO SUBMIT PARAMETERS
    /*
    function cineBoxFnProcessUserCard(e,defer,deferfunc){
        var robj,rtype,req,query;
        var bt_token,bt_number,bt_cvv,bt_month,bt_year,bt_name,bt_zip,
            bt_token_el,bt_number_el,bt_cvv_el,bt_month_el,bt_year_el,bt_name_el,bt_zip_el,
            newel,cur,id;

        robj = e.target.getAttribute('cinerootobject');
        rtype = e.target.getAttribute('type');
        req = 'cardinfo';
        query = (rtype=='submit')?'addcard':'deletecard';
        rtype = 'view';

        bt_token_el = document.getElementById('cinereq_bttoken');
        bt_number_el = document.getElementById('cinereq_number');
        bt_cvv_el = document.getElementById('cinereq_cvv');
        bt_month_el = document.getElementById('cinereq_expiration_month');
        bt_year_el = document.getElementById('cinereq_expiration_year');
        bt_name_el = document.getElementById('cinereq_cardholder_name');
        bt_zip_el = document.getElementById('cinereq_postal_code');

        var elarr=[bt_token_el,bt_number_el,bt_cvv_el,bt_month_el,bt_year_el,bt_name_el,bt_zip_el];

        bt_token = bt_token_el.value;
        bt_number = bt_number_el.value.toString();
        bt_cvv = bt_cvv_el.value;
        bt_month = bt_month_el.value;
        bt_year = bt_year_el.value;
        bt_name = bt_name_el.value;
        bt_zip = bt_zip_el.value;

        for(var i=0;i<7;i++){
            cur=elarr[i];
            id=cur.id;
            cur.setAttribute('id','tmp'+id);
            cur.className = 'cinedatatmp';
            newel=document.createElement('input');
            newel.setAttribute('type','hidden');
            newel.setAttribute('id',id);
            newel.className = 'cinedata';
            newel.value = cur.value;
            if(id=='cinereq_number'){
                newel.value = cur.value.slice(-4);}
            if(id=='cinereq_cvv' || id=='cinereq_cardholder_name'){
                newel.value = '';}
            cur.parentNode.insertBefore(newel,cur);
        }

        if(query=='addcard' || query=='editcard'){
            var client = new braintree.api.Client({clientToken:bt_token});
            client.tokenizeCard({number: bt_number, cardholderName: bt_name,expirationMonth: bt_month,
                expirationYear: bt_year,cvv: bt_cvv, billingAddress: {postalCode: bt_zip}
            },function(err,nonce){
                //route: function(e,manual,rootobj,rtype,require,query,ext,defer,deferfunc,ident,attr){
                var attr={reqObject:document.getElementById(robj),reqType:rtype,reqAction:req,reqQuery:query,reqExtData:nonce,execFn:deferfunc,reqUserKey:true,execClose:true};
                $cine().rexec.route(e,true,robj,rtype,req,query,nonce,true,deferfunc,0,attr);
                // -----------------e,manu,root,rtype,req,query,ext  ,defer,deferfnc,ident,attr
            });
        } else {
            var attr={reqObject:document.getElementById(robj),reqType:rtype,reqAction:req,reqQuery:query,reqExtData:'',execFn:deferfunc,reqUserKey:true,execClose:true};
            $cine().rexec.route(e,true,robj,rtype,req,query,'',true,deferfunc,0,attr);
        }

    }*/
    // LOAD EXTERNAL LIBRARY
    function cineBoxFnloadScript(url,obj,callback,defer,deferfunc){
        var s = document.createElement("script");
        s.type = "text/javascript";
        if (s.readyState){
            s.onreadystatechange = function(){
                if (s.readyState == "loaded" || s.readyState == "complete"){
                    s.onreadystatechange = null;
                    callback(obj,defer,deferfunc);
                }
            };
        } else {
            s.onload = function(){callback(obj,defer,deferfunc);};
        }
        s.src = url;
        document.getElementsByTagName("head")[0].appendChild(s);
    }
    // DOM TREE TRAVERSE
    function cineBoxFnDomTraverse(obj,objclass){

        var cur,ti,curc,up,uppn;
        var t=['NONE','INPUT','SELECT','TEXTAREA'];
        var cl=false;

        if(objclass != null || typeof objclass !== 'undefined'){
            objclass = objclass.split(':');
            if(objclass[0]!=='*'){
                t=[objclass[0].toUpperCase()];}
            cl=objclass[1];
        }

        var d=[];

        if(obj!=null){

            var s=["none",obj.id];
            var c=obj.childNodes;
            var k=c.length;
            var z=0;

            for(var i=0;i<k;i++){
                cur = c[i];

                if(cur.nodeType != 3){

                    if(cur.id == null || cur.id == ''){
                        ti = new Date();
                        ti=ti.getHours()+''+ti.getMinutes()+''+ti.getSeconds()+''+ti.getMilliseconds()+''+Math.round((999)*Math.random());
                        cur.setAttribute('id','cine-fdt' + ti);
                    }

                    if(!s.indexElm(cur.id)){

                        if(!t.indexElm(cur.tagName)){

                            curc = cur.childNodes;
                            if(curc.length == 1 && curc[0].nodeType != 3){
                                c=curc; i=-1; k=c.length;
                            }
                            if(curc.length > 1){
                                c=curc; i=-1; k=c.length;
                            }
                        }
                        else {

                            if(cl){ // CLASS SPECIFIED
                                if(cineBoxFnReturnClass(cur.className,cl)){
                                    d.push(cur);}
                            }
                            else{ // NOT CLASS SPECIFIED
                                d.push(cur);}
                        }
                    }
                    s.push(cur.id);
                }
                if(i==(k-1)){
                    uppn=cur.parentNode;
                    if(uppn != obj){
                        up = uppn.parentNode;
                        c=up.childNodes;
                        i=-1;
                        k=c.length;
                    }
                }
                z++; if(z>1000) break;
            }

            return d;
        }
    }
    function cineBoxFnDomCreateEl(type,id,cls,disp,cont,sel,seldef){
        var obj,l,i,t,c,cur; obj=document.createElement(type);
        if(id.length==0){
            t = new Date();
            t=t.getHours()+''+t.getMinutes()+''+t.getSeconds()+''+t.getMilliseconds()+''+Math.round((999)*Math.random());
            obj.setAttribute('id','cine-id'+String(t));
        } else
            obj.setAttribute('id',id);
        if(cls.length > 0){
            c = cls.split(' ');
            l = c.length;
            if(l>1){
                for(i=0;i<l;i++){cur=c[i];
                    if(cur[i].length>0 && i>0)
                        obj.className += ' '+cur[i];
                    else
                        obj.className += cur[i];
                }
            } else
                obj.className += cls;
        }
        if(disp=='block' || disp=='inline-block' || disp=='none') obj.style.display = disp;
        if(type=='input' || type=='textarea') obj.value = cont; else obj.innerHTML = cont;
        if(type=='select'){
            c = cont.split(',');
            var sc='',sl,vl='';
            for(i=0;i<c.length;i++){
                sl='';
                if(sel) vl=i; else vl=c[i]; if(seldef==vl) sl='selected="selected"';
                sc+='<option '+sl+' value="'+vl+'">'+c[i]+'</option>';
            }
            obj.innerHTML = sc;
        }
        return obj;
    }

    // ---------------------------------------------------------
    /* CLIENT SIDE FUNCTIONS

     */
    /*
    function cineBoxParseGeoQuery(o){
        if(o.status=="OK"){
            var t={sublocality_level_1:'long_name',locality:'long_name',administrative_area_level_1:'long_name',route:'long_name',postal_code:'long_name'};
            var r={sublocality_level_1:false,locality:false,administrative_area_level_1:false,route:false,postal_code:false};
            var l={street_address:false,neighborhood:false};
            var s=o.results;
            var c,e,a;

            for(var k in s){

                if(s.hasOwnProperty(k)){
                    c=s[k].types[0];
                    if(cineObjectIndex(l,c))l[c]=s[k].address_components;
                }
            }

            for(k in l){
                if(l.hasOwnProperty(k)){
                    c=l[k];
                    for(var d in c){
                        e=c[d];
                        if(e.hasOwnProperty('types')){
                            a=e.types[0];
                            if(cineObjectIndex(r,a)){r[a]=e[t[a]];}
                        }
                    }
                }
            }
            return r;
        }
        return false;
    }
    function cineBoxObjToCommaString(obj){
        var res='';
        for(var k in obj){
            if(obj.hasOwnProperty(k) && obj[k]){
                res+=obj[k]+', ';
            }
        }
        res=res.split('');
        //var l=(res.length-1);
        res.pop();
        res.pop();
        res=res.join('');
        console.log(res);
        return res;
    }
    */

    // ---------------------------------------------------------
    /* CLIENT SIDE FUNCTIONS
    function cineBoxFnObjRestoList(obj,pos){

        var list=document.getElementById('cine_fn_resto_list');
        if(list!=null && obj.length>0){

            obj=JSON.parse(obj);
            var m=0.621;
            var y=0.915;
            var v=0;
            var vm=0;
            var t;
            var r=obj.result;
            var g=obj.geo;
            var d=0;
            var n=[];
            var adr,ret,inr,btn;
            var htm='';
            if(obj.success){

                for(var k in g){
                    if(g.hasOwnProperty(k)){
                        d=cineBoxFnRealDist(pos.coords.latitude,pos.coords.longitude,g[k].lat,g[k].lng);
                        n.push({d:d,i:k});
                    }
                }

                n.sort(function(a, b){
                    if (a.d > b.d){return 1;}if(a.d < b.d){return -1;}return 0;
                });

                var lt=cineCreateDomEl('table','','cartTable table-responsive','');

                for(var i in n){

                    ret=null;

                    if(n.hasOwnProperty(i)){

                        var cur=n[i].i;
                        v=n[i].d;
                        vm=(v*m).toFixed(2);
                        t='miles';
                        if(vm<0.5){
                            vm=((v*1000)/y).toFixed(0); t='yards';
                        }
                        adr = r[cur].address;

                        ret=cineCreateDomEl('tr','citem_'+r[cur],'CartProduct','');
                        ret.setAttribute('cineident',r[cur].name);

                        htm ='<span class="cine_fn_resto_e_title"> '+r[cur].name
                        +' </span> <br/>  <span > '+obj.type[r[cur].type]
                        +' </span> <br/> <span class="cine_fn_resto_e_desc"> '+r[cur].description
                        +' </span> <br/>'+adr.address_street+' <br/> '+adr.address_city+'';

                        inr=cineCreateDomEl('td','','col-xs-11 pull-right cine_fn_resto_list_e',htm,ret);
                        inr.setAttribute('cineident',r[cur].name);

                        htm ='<span class="cine_fn_resto_e_dist"> '+vm+' '+t+' away </span>';

                        inr=cineCreateDomEl('td','','cine_fn_resto_list_e',htm,ret);
                        inr.setAttribute('cineident',r[cur].name);

                        btn=cineCreateDomEl('button','','btn btn-danger','Order',inr);
                        btn.addEventListener('click',function(e){cineBoxFnRestoOrder(e,this.id);}.bind({id:cur}));

                        lt.appendChild(ret);

                    }

                }

                list.appendChild(lt);
            }
        }
        return false;
    }

    function cineBoxFnRestoOrder(e,r){

        e.preventDefault();
        var c=new cineBox();
        var t=cineBoxFnDomObject(e.target,'DIV');
        t=t.getAttribute('cineident');

        var param = {
            reqType:'man',
            reqIdent:r,
            reqAction:'restoinfo',
            reqQuery:'getrestomenu',
            reqEvent: $cine().resto.getMenu,
            reqEventData: true,
            reqTitle:'',
            reqAppTitle:t,
            reqBtnName:'',
            reqSpecial:null
        };
        c.rexec.getManual(r,param);
    }

    function cineBoxFnRestoOrderMenu(e,d){
        var mid=e.getAttribute('modalid');
        var cid=document.getElementById('cinecont'+mid);
        cid.style.display='block';
        if(d.success){
            var mnu=cineBoxFnRestoRendrMenu(d.result,1,e,false,d.resto);
            cid.appendChild(mnu);
            mnu = document.createElement('input');
            mnu.setAttribute('id','cine_fn_resto_menu');
            mnu.setAttribute('type','hidden');
            mnu.setAttribute('cineident',d.resto);
            mnu.setAttribute('value','');
            mnu.value= JSON.stringify(d.result);
            cid.appendChild(mnu);
            mnu = document.createElement('input');
            mnu.setAttribute('id','cine_fn_resto_sidemenu');
            mnu.setAttribute('type','hidden');
            mnu.setAttribute('cineident',d.resto);
            mnu.setAttribute('value','');
            mnu.value= JSON.stringify(d.side);
            cid.appendChild(mnu);
        } else {
            var mf=document.getElementById('cine_fn_view');
            var b=cineBoxFnRestoMenuBack(false,false,mf,e,{cart:false});
            e.insertBefore(b, e.childNodes[1]);
            b=document.createElement('div');
            b.innerHTML = 'Restaurant has not have menu yet';
            cid.appendChild(b);
        }
    }
    function cineBoxFnRestoRendrMenu(d,l,g,sg,i){
        var e=document.createElement('div');
        var mf,n,k,b,cur,cur2,prev;
        e.setAttribute('id','menulevel'+l);
        e.setAttribute('cinelevel',l);

        if(!d){

            d=document.getElementById('cine_fn_resto_menu');
            d=d.value;
            if(d[0]!='{' && d.length<2){
                n=document.createElement('div');
                n.className='cine_fn_resto_menu_e';
                n.innerHTML='<label> Menu list is empty </label>';
                e.appendChild(b);
                e.appendChild(n);
            } else {
                d=JSON.parse(d);
            }
        }

        if(typeof d==='object'){
            if(l==1){
                mf=document.getElementById('cine_fn_view');

                b=cineBoxFnRestoMenuBack(false,false,mf,g,true);
                e.appendChild(b);

                for(k in d){
                    if(d.hasOwnProperty(k)){
                        n=document.createElement('div');
                        n.className='cine_fn_resto_menu_e';
                        n.innerHTML='<label>'+d[k].title+'</label>';
                        n.addEventListener('click',function(e){cineBoxFnRestoRendrMenu(false,this.l,this.g,false,this.r)}.bind({g:k,l:l+1,r:i}));
                        e.appendChild(n);
                    }
                }

            }else if(l==2){

                cur = d[g].include;

                prev=document.getElementById('menulevel'+(l-1));
                b=cineBoxFnRestoMenuBack(false,false,prev,e,true);
                e.appendChild(b);

                n=document.createElement('div');
                n.className='cine_fn_resto_menu_l';
                n.innerHTML=''+d[g].title+'';
                e.appendChild(n);

                for(k in cur){
                    if(cur.hasOwnProperty(k)){
                        n=document.createElement('div');
                        n.className='cine_fn_resto_menu_e';
                        n.innerHTML='<label>'+cur[k].title+'</label>';
                        n.addEventListener('click',function(e){cineBoxFnRestoRendrMenu(false,this.l,this.g,this.sg,this.r)}.bind({g:g,sg:k,l:l+1,r:i}));
                        e.appendChild(n);
                    }
                }

                prev.style.display = 'none';
                prev.parentNode.appendChild(e);

            }else if(l==3){

                cur = d[g].include;
                cur2= cur[sg].include;

                prev=document.getElementById('menulevel'+(l-1));
                b=cineBoxFnRestoMenuBack(false,false,prev,e,true);
                e.appendChild(b);
                n=document.createElement('div');
                n.className='cine_fn_resto_menu_l';
                n.innerHTML=''+cur[sg].title+'';
                e.appendChild(n);

                for(k in cur2){
                    if(cur2.hasOwnProperty(k)){
                        n=document.createElement('div');
                        n.className='cine_fn_resto_menu_e cine_fn_resto_menu_ei';
                        n.innerHTML='<div class="cine_fn_resto_menu_eit"><label>'+cur2[k].title+'</label><br/><span style="padding-top:5px; font-size:12px;">'+cur2[k].desc+'</span></div><div class="cine_fn_resto_menu_eip"><span style="float: right; padding-right: 5px;">$ '+cur2[k].price+'</span></div>';
                        n.addEventListener('click',function(e){cineBoxFnRestoItemOrder(this.i,this.d,this.g,this.r)}.bind({i:k,d:cur2[k],g:e,r:i}));
                        e.appendChild(n);
                    }
                }

                prev.style.display = 'none';
                prev.parentNode.appendChild(e);

            }
        }
        return e;
    }
    */

    /* function cineBoxFnRestoItemOrder(id,d,p,r){
        var e,b,n,sn,k;

        e=document.createElement('div');
        e.setAttribute('id','menulevel4');

        b=cineBoxFnRestoMenuBack(false,false,p,e,{cart:true});
        e.appendChild(b);

        n=document.createElement('div');
        n.className='cine_fn_resto_menu_i_d';

        var dval={i:id,p:Number(d.price),n:d.title,d:d.desc,q:1,s:false};

        var ectrl=document.createElement('input');
        ectrl.setAttribute('id','cine_fn_mli'+id);
        ectrl.setAttribute('type','hidden');
        ectrl.setAttribute('value',JSON.stringify(dval));
        n.appendChild(ectrl);

        sn=document.createElement('div');
        sn.className='cine_fn_resto_menu_i_t';
        sn.innerHTML='<h4>'+d.title+'</h4>';
        n.appendChild(sn);

        sn=document.createElement('div');
        sn.className='cine_fn_resto_menu_i_i';
        sn.innerHTML=''+d.desc+'';

        n.appendChild(sn);
        e.appendChild(n);

        n=document.createElement('div');
        n.className='cine_fn_resto_menu_i_p';
        sn=document.createElement('div');
        sn.setAttribute('id','cine_fn_mlip'+id);
        sn.innerHTML='<h4>$ '+d.price+'</h4>';
        n.appendChild(sn);

        sn=document.createElement('div');
        sn.className='cine_fn_resto_menu_i_o';
        sn.setAttribute('id','cine_fn_mlio'+id);
        snb=document.createElement('button');
        snb.className='cine_fn_color_btn';
        snb.setAttribute('type','button');
        snb.innerHTML="<div class='cart_fn_cartglyph'></div> Add to order";
        snb.setAttribute('id','cine_item'+id);
        snb.setAttribute('cineident',id);
        snb.addEventListener('click',function(e){cineBoxFnRestoItemToOrder(e,this.id,this.il,this.ll,this.r)}.bind({id:id,il:e,ll:p,r:r}));
        sn.appendChild(snb);

        n.appendChild(sn);
        e.appendChild(n);

        var s = d.include;
        s=s.split(',');
        var ds = document.getElementById('cine_fn_resto_sidemenu');
        ds = ds.value;
        if(ds.length>2 && ds[0]=='{')
            ds=JSON.parse(ds);
        else
            ds={};

        var i = 0;
        var ev=[];
        var ea=[];

        for(k in s){
            if(s.hasOwnProperty(k)){

                if(cineObjectIndex(ds,s[k])){

                    var cur=ds[s[k]];
                    var sv=cineBoxFnRestoSideRender(cur,s[k],id);

                    if(sv.type){
                        n=cineBoxLabelFrame({title:cur.name,elems:[sv.elem]});
                        e.appendChild(n);
                    }
                    else
                        ea.push(sv.elem);

                }
            }
        }

        n=cineBoxLabelFrame({title:'Add your favorites',elems:ea});
        e.appendChild(n);

        sn=document.createElement('textarea');
        sn.setAttribute('id','cine_fn_mlit'+id);
        sn.style.cssText = 'width:100%; height:80px; resize:none';
        n=cineBoxLabelFrame({title:'Additional information',elems:[sn]});


        e.appendChild(n);

        p.style.display = 'none';
        p.parentNode.appendChild(e);

    }
    */

    /*
    function cineBoxFnRestoSideRender(d,s,id){

        var out={type:false,elem:false};

        var ret,tr1,tr2,tr3,td;
        var vars='';
        var k,l=0,mode=false;
        var dv = d.variant;
        var dk = Object.keys(dv);
        var tdp=[];
        var tdb=[];

        if(dk.length>0){

            for(k in dv){
                if(dv.hasOwnProperty(k)){

                    if(Number(dv[k])>0){
                        mode=true;
                        tdb.push('<input id="'+s+l+'" class="cine_fn_color_btn" type="button" value="'+k+'" number="'+l+'" scope="'+s+'" price="'+dv[k]+'" baseprice="'+d.price+'" basename="'+d.name+'" ident="'+id+'" style="width:95%" onclick="$cine().menu.sideSet(this);" />');
                        tdp.push('<div>$ '+dv[k]+'</div>');
                    }
                    else if(Number(dv[k])==0){
                        tdb.push('<input id="'+s+l+'" class="cine_fn_color_btn" type="button" value="'+k+'" number="'+l+'" scope="'+s+'" price="0" baseprice="'+d.price+'" basename="'+d.name+'" ident="'+id+'" style="width:95%" onclick="$cine().menu.sideSet(this);"  />');
                        tdp.push('<div> Free </div>');
                    } else {
                        tdb.push('<input id="'+s+l+'" class="cine_fn_color_btn" type="button" value="'+k+'" number="'+l+'" scope="'+s+'" price="0" baseprice="'+d.price+'" basename="'+d.name+'" ident="'+id+'" style="width:95%" onclick="$cine().menu.sideSet(this);"  />');
                        tdp.push('<div></div>');
                    }
                    l++;

                }
            }

            ret=document.createElement('table');
            ret.style.width = '100%';
            var pts=l;
            tr1=document.createElement('tr');
            td=document.createElement('td');
            td.setAttribute('colspan',pts);
            td.className = 'info';
            td.innerHTML=d.desc;
            tr1.appendChild(td);
            ret.appendChild(tr1);

            // If all parts of side could have a price
            if(mode && Number(d.price)>0){

                tr1=document.createElement('tr');
                td=document.createElement('td');
                td.setAttribute('colspan',pts);
                td.innerHTML='$ '+d.price;
                tr1.appendChild(td);
                ret.appendChild(tr1);

            }
            // If only variants has price
            else if(mode && Number(d.price)==0) {

                tr1=document.createElement('tr');
                td=document.createElement('td');
                td.setAttribute('colspan',pts);
                td.innerHTML='Free';
                tr1.appendChild(td);
                ret.appendChild(tr1);

            }
            // If any other variant (no price at variants and price at basic, or all equal to zero)

            tr2=document.createElement('tr');
            tr3=document.createElement('tr');
            for(k=0;k<l;k++){
                td=document.createElement('td');
                td.style.width=Math.floor(100/pts)+'%';
                td.innerHTML=tdb[k];
                tr2.appendChild(td);
                td=document.createElement('td');
                td.style.width=Math.floor(100/pts)+'%';
                td.innerHTML=tdp[k];
                tr3.appendChild(td);
            }
            ret.appendChild(tr2);
            ret.appendChild(tr3);

            out.type=true;
            out.elem=ret;

        }
        // If no variants at all (only button to add)
        else {

            vars='<input id="'+s+'1" class="cine_fn_color_btn" type="button" scope="'+s+'" number="1" ident="'+id+'" baseprice="'+d.price+'" basename="'+d.name+'" value="'+ d.name +'" style="width:95%" onclick="$cine().menu.sideSet(this);" />';
            ret=document.createElement('table');
            ret.style.width = '100%';

            tr1=document.createElement('tr');
            td=document.createElement('td');
            td.setAttribute('colspan',3);
            td.className = 'info';
            td.innerHTML=d.desc;
            tr1.appendChild(td);
            ret.appendChild(tr1);

            tr1=document.createElement('tr');
            td=document.createElement('td');
            td.style.width='33%';
            td.innerHTML = vars;
            tr1.appendChild(td);

            if(Number(d.price)>0)
                price='$ '+ d.price;
            else
                price= 'Free';

            td=document.createElement('td');
            td.innerHTML = price;
            td.style.width='33%';
            tr1.appendChild(td);

            td=document.createElement('td');
            td.style.width='33%';
            td.innerHTML = 'Decline';
            tr1.appendChild(td);

            ret.appendChild(tr1);

            out.elem=ret;

        }
        return out;
    }
    */
    function cineBoxFnRestoMenuSideSet(e){

        var t=0;
        var pn=null;
        var pt=true;

        var s= e.getAttribute('scope');
        var p= e.getAttribute('price');
        var bp=Number(e.getAttribute('baseprice'));
        var n= e.getAttribute('number');
        var bn=e.getAttribute('basename');

        var ip=cinePriceTitle;
        var d=cinePriceObject;
        //{i:id,p:d.price,n:d.title,d:d.desc,q:1,s:false}
        if(d){
            var ndata={n:bn,v:e.value,i:n,p:bp,p2:p,q:0};

            if(d.s){

                var cur=d.s;
                var fnd=false;

                var x=0;
                for(var k in cur){
                    if(cur.hasOwnProperty(k)){

                        x++;
                        if(k==s){

                            fnd=true;
                            pn=cur[k].i;
                            if(pn!=n)
                                cur[k]=ndata;
                            else {
                                delete cur[k];
                                pt=false;
                                x--;
                            }

                        }
                    }
                }

                if(!fnd){
                    cur[s] = ndata;
                    x++;
                }

                if(x<1) d.s=false;

                t=cineBoxFnRestoIPriceCnt(d);
                ip.textContent='$ '+t;
                cinePriceObject=d;

            } else {

                var elm = {};
                elm[s] = ndata;
                d.s = elm;

                t=cineBoxFnRestoIPriceCnt(d);
                ip.textContent='$ '+t;
                cinePriceObject=d;

            }

            if(pn!=null)
                cineSideOptions[s+pn].className = 'cine_fn_color_btn cine_side_btn';
            if(pt)
                e.className += ' cine_btn_active';

        }
    }

    // -------------------------------------------------
    // PROCESS ITEM OREDER TO SERVER
    // -------------------------------------------------
    /*
    function cineBoxFnRestoItemToOrder(e,id,item,list,r){

        var pl=list.getAttribute('cinelevel');
        pl=document.getElementById('menulevel'+(parseInt(pl)-1));

        var c=new cineBox();
        var t=cineBoxFnDomObject(e.target);

        var data=document.getElementById('cine_fn_mli'+id);
        var spec=document.getElementById('cine_fn_mlit'+id);
        var cart=document.getElementById('cine_fn_base_cart_items');

        var param = {
            reqType:'man',
            reqIdent:r,
            reqAction:'restoorder',
            reqQuery:'putitemtoorder',
            reqEvent: cineBoxFnRestoOrderPut,
            reqEventData: true,
            reqWindow: false,
            reqTitle:t+' Menu',
            reqBtnName:'Create Order',
            reqSpecial:spec.value,
            reqData:data.value,
            reqExt:cart.value
        };
        c.rexec.getManual(t,param);

        if(pl!=null){
            pl.style.display='block';
        }
        item.parentNode.removeChild(item);
        list.parentNode.removeChild(list);
    }
    */
    function cineBoxFnRestoOrderPut(e,obj,elem){

        if(obj.success){
            var res=obj.result;

            if(typeof elem==='undefined' || !elem){
                var cart=document.getElementsByClassName('cine_fn_cart');
                for(var k in cart){
                    var cur=cart[k];
                    if(typeof cur==='object'){
                        if(!cineBoxFnReturnClass(cur.className,'cine_fn_cart_active')){
                            cur.className = 'cine_fn_cart cine_fn_cart_active';
                            cur.addEventListener('click',function(e){cineBoxFnRestoOrderList(e,this.r);}.bind({r:res}));
                        }
                    }
                }
            } else {
                elem.className = 'cine_fn_cart cine_fn_cart_active';
                elem.addEventListener('click',function(e){cineBoxFnRestoOrderList(e,this.r);}.bind({r:res}));
            }

            var fc=document.getElementById('cine_fn_base_cart_items');
            fc.value = JSON.stringify(res);

            if(window.localStorage!=='undefined'){
                window.localStorage.setItem('userorder',JSON.stringify(res));
            }

        }
        //console.log(obj);
    }

    // -------------------------------------------------------
    // | RESTAURANT ORDER LIST QUERY AND OPERATIONS
    // -------------------------------------------------------
    function cineBoxFnRestoOrderList(e,r){

        var c=new cineBox();
        var id= r.order;
        var t=cineBoxFnDomObject(e.target);

        var param = {
            reqType:'man',
            reqIdent:id,
            reqAction:'restoorder',
            reqQuery:'getorderlist',
            reqEvent: cineBoxFnRestoOrderListRender,
            reqEventData: true,
            reqWindow: true,
            reqTitle:'',
            reqAppTitle:"Your order",
            reqBtnName:'Proceed checkout',
            reqSpecial:'',
            reqData: r.key,
            reqExt:''
        };
        c.rexec.getManual(t,param);

    }
    // Window where all order checkout fields are created
    function cineBoxFnRestoOrderListRender(e,obj){

        var m=e.getAttribute('modalid');
        var c=document.getElementById('cinecont'+m);

        var b=cineBoxFnRestoMenuBack(false,false,false,e);
        e.insertBefore(b,c);

        var ec=null;

        if(obj.success){
            ec=cineBoxFnRestoOrderItemList(obj.result,obj.order,obj.key);
        }

        if(ec!=null)
            var cr=cineBoxLabelFrame({elems:ec,title:'Ordered Items'});

        c.appendChild(cr);

        ec=cineBoxFnRestoOrderItemSumm(obj.price);
        c.appendChild(ec);

        var eb=document.createElement('a');
        eb.className='cine_fn_color_btn';
        eb.innerHTML='Order checkout';
        eb.style.cssText = 'margin-top:20px !important;';
        eb.addEventListener('click',function(ev){cineBoxFnRestoOrderCheckout(ev,this.id,this.win)}.bind({id:obj.order,win:e}));
        c.appendChild(eb);

        cineBoxFnModalAfterLoad(e);

    }
    // Create list of ordered items with delete buttons
    function cineBoxFnRestoOrderItemList(r,id,key,fn,ds,ss){

        var i=0,cur,itm,elm,ec,et,eb,er,sd,sc;
        if(typeof fn==='undefined')fn=true;
        if(typeof ds==='undefined')ds=true;
        if(typeof ds==='undefined')ss=false;

        ec=document.createElement('table');
        ec.className='cine_fn_rol_i';
        ec.setAttribute('id','c_order_i_'+id);

        var p=0,p0=0,p1=0,p2=0;

        for(var k in r){
            if(r.hasOwnProperty(k)){

                cur=r[k];
                itm=Object.keys(cur);
                elm=cur[itm[0]];

                er=document.createElement('tr');
                er.className='cine_fn_rol_i_tr';
                er.setAttribute('id','cie_'+k);

                et=document.createElement('td');
                et.className='cine_fn_rol_i_td';
                et.setAttribute('valign','top');
                et.style.width='60%';

                bn = document.createElement('div');
                bn.className = 'cine_fn_iedit_cnt';
                if(fn){
                    bn.innerHTML = '<div class="cine_fn_iedit_btn"><i class="fa fa-cutlery"></i></div> &nbsp;' + elm.n + '';
                    bn.addEventListener('click', function (e) {
                        cineRestoOrderAddSide(e, this)
                    }.bind(k));
                }else
                    bn.textContent = elm.n;

                et.appendChild(bn);

                h='';

                var sext=false;
                if(ss){
                    sd=elm.s;
                    if(sd){

                        h+='<div>';
                        var skey=Object.keys(sd);
                        var lkey=skey[skey.length-1];

                        for(var s in sd){
                            sc=sd[s];
                            p0=0;
                            p1=Number(sc['p']);
                            p2=Number(sc['p2']);
                            if(s!=lkey)
                                cn="cine_fn_rol_i_tdsi";
                            else
                                cn="cine_fn_rol_i_tdsil";

                            if(p1>0)
                                p0+=p1;
                            if(p2>0)
                                p0+=p2;
                            if(p0>0)
                                n3='$ '+p0+'';
                            else
                                n3='';

                            n2=' &gt;'+sc['v']+'';
                            if(sc['p2']==null){
                                n2='';
                            }
                            h+='<div class="cine_fn_rol_i_tds"><div class="'+cn+'">'+sc['n']+' '+n2+'</div><div class="cine_fn_rol_i_tdsp">'+n3+'</div></div>';

                            sext=true;
                        }
                        h+='</div>';

                    }
                }

                et.insertAdjacentHTML('beforeend',h);
                er.appendChild(et);
                if(!sext){er.className='cine_fn_rol_i_trl'}

                et=document.createElement('td');
                et.className='cine_fn_rol_i_tq';
                et.setAttribute('id','ciq_'+k);
                et.setAttribute('valign','top');
                et.innerHTML='<h4> x&nbsp;'+elm.q+'</h4>';
                er.appendChild(et);

                et=document.createElement('td');
                et.className='cine_fn_rol_i_tp';
                et.setAttribute('valign','top');
                et.innerHTML='<h4>$ '+elm.p+'</h4>';
                er.appendChild(et);

                if(fn){
                    et=document.createElement('td');
                    et.className='cine_fn_rol_i_tb';
                    et.setAttribute('valign','top');
                    eb=document.createElement('button');
                    eb.className='cine_fn_color_btn cine_common_btn';
                    eb.innerHTML='<div class="c-btn-delete"></div>';
                    eb.addEventListener('click',function(e){cineBoxFnRestoOrderIdelete(e,this.id,this.nid,this.oid,this.key)}.bind({id:k,nid:itm[0],oid:id,key:key}));
                    et.appendChild(eb);
                    er.appendChild(et);
                }

                ec.appendChild(er);

                info=cur['info'];
                if(info!=null && info.length>0){
                    er=document.createElement('tr');
                    et=er.insertCell(-1);
                    et.setAttribute('colspan',4);
                    et.innerHTML='<div class="highlite-black" style="margin-top: 4px;">Note: '+info+'</div>';
                    ec.appendChild(er);
                }

                i++;
            }
        }
        if(i>0) return ec; return false;
    }

    // Create a summary for item checkout
    function cineBoxFnRestoOrderItemSumm(p,t,u){

        $cine().order.countPrices(p,t,u);

        var i=0;
        //if(t==0 || typeof t==='undefined')
        //t=8.875;
        var ec=document.createElement('table');
        ec.className='cine_fn_rol_i_total font16 full-width';
        ec.setAttribute('id','cine_fn_order_total');

        var er=ec.insertRow(i);
        er.className='cine_fn_rol_i_total_s';
        var et=er.insertCell(0);
        et.textContent='Subtotal';
        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_itotal');
        et.className='cine_fn_rol_i_price';
        et.textContent='$ '+ $cine().order.price(astype.string);
        i++;

        //var pt=((p/100)*(t));

        er=ec.insertRow(i);
        er.className='cine_fn_rol_i_total_s';
        et=er.insertCell(0);
        et.textContent='Tax';

        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_ttotal');
        et.className='cine_fn_rol_i_price';
        et.textContent='$ ' + $cine().order.tax(astype.string);
        i++;

        //var pf=Number(pt)+Number(p);
        if(typeof u !=='undefined'){

            er=ec.insertRow(i);
            er.className='cine_fn_rol_i_total_s';
            et=er.insertCell(0);
            et.textContent='Customer tip';
            et=er.insertCell(1);
            et.className='cine_fn_rol_i_price';
            et.setAttribute('id','cine_fn_rol_utotal');

            et.textContent='$'+ $cine().order.tip(astype.string);
            i++;
        }

        er=ec.insertRow(i);
        er.style.cssText='border-top:1px solid #cccccc; padding-top:4px;';
        er.className='cine_fn_rol_i_total_s';

        et=er.insertCell(0);
        et.innerHTML='<h4>Total</h4>';
        et=er.insertCell(1);
        et.setAttribute('id','cine_fn_rol_ototal');
        et.className='cine_fn_rol_i_price';
        et.innerHTML='<h4>$ '+$cine().order.total(astype.string)+'</h4>';

        return ec;
    }

    function cineBoxFnRestoOrderIdelete(e,id,nid,oid,key){

        var c=new cineBox();
        var t=cineBoxFnDomObject(e.target);

        var data=JSON.stringify({order:oid,item:id,subitem:nid});

        var param = {
            reqType:'man',
            reqIdent:oid,
            reqAction:'restoorder',
            reqQuery:'delitemfromorder',
            reqEvent: cineBoxFnRestoOrderIdeleteProc,
            reqEventData: true,
            reqWindow: false,
            reqTitle:'',
            reqBtnName:false,
            reqSpecial:'',
            reqData: data,
            reqExt:key
        };
        c.rexec.getManual(t,param);

    }

    function cineBoxFnRestoOrderIdeleteProc(e,r){

        var data= r.result;

        var elem= document.getElementById('cie_'+data.item);
        var elemq= document.getElementById('ciq_'+data.item);

        var ttlp= document.getElementById('cine_fn_rol_itotal');
        var ttlt= document.getElementById('cine_fn_rol_ttotal');
        var ttlo= document.getElementById('cine_fn_rol_ototal');

        var ttlu= document.getElementById('cine_fn_rol_utotal');

        if(data.q>0)
            elemq.innerHTML='<h4> x&nbsp;'+data.q+'</h4>';
        else
            elem.parentNode.removeChild(elem);

        var price=Number(data.p);
        var tax=Number(data.tax);
        tax=(tax==0)?8.875:tax;
        var tip=Number(data.tip);

        var pt=(price/100)*tax;
        var u=((price+pt)/100)*tip;

        if(ttlu!=null){
            ttlu.textContent='$ '+ u.toFixed(2);
        }

        var pto=pt+price+u;

        ttlp.textContent='$ '+price;
        ttlt.textContent='$ '+pt.toFixed(2);
        ttlo.textContent='$ '+pto.toFixed(2);

        if(cinePriceListObject!=null){
            cinePriceListObject.innerHTML=(data.p).toFixed(2);
        }

    }

    function cineBoxFnRestoOrderCheckout(e,id,win){

        var c=new cineBox();

        t=win.id;
        if(typeof e!=='undefined')
            var t=cineBoxFnDomObject(e.target);

        var uid=0;
        var ukey=0;

        var param = {
            reqType:'man',
            reqIdent:id,
            reqAction:'restoorder',
            reqQuery:'getordercheckout',
            reqEvent: cineBoxFnRestoOrderRndrCheckout,
            reqEventData: true,
            reqWindow: true,
            reqTitle:'',
            reqAppTitle:"Order checkout",
            reqBtnName:'Confirm checkout',
            reqSpecial:win.id,
            reqData:'',
            reqExt:'',
            reqUserKey:true,
            reqRepeat:function(e){cineBoxFnRestoOrderCheckout(e,this.id,this.win)}.bind({id:id,win:win})
        };

        c.rexec.getManual(t,param);
    }

    function cineBoxFnRestoOrderRndrCheckout(e,obj){

        var m=e.getAttribute('modalid');
        var c=document.getElementById('cinecont'+m);
        var mp=e.getAttribute('cinespecial');
        mp=document.getElementById('cinemodal'+mp);

        var b=cineBoxFnRestoMenuBack(false,false,false,e);
        e.insertBefore(b,c);

        if(obj.success){

            var tb=document.createElement('table');
            var tr=document.createElement('tr');
            var td=document.createElement('td');
            td.innerHTML=' <h4> ORDER SUMMARY </h4>';

            var res=obj.result;
            //for(var k in res){
            ec=document.createElement('table');
            ec.className='cine_fn_rol_i';

                for(var k in res){
                    if(res.hasOwnProperty(k)){

                        cur=res[k];
                        itm=Object.keys(cur);

                        er=document.createElement('tr');
                        er.className='cine_fn_rol_i_tr';
                        er.setAttribute('id','cie_'+k);

                        et=document.createElement('td');
                        et.className='cine_fn_rol_i_td';
                        et.innerHTML='<h4>'+cur[itm[0]].n+'</h4><h5>'+cur[itm[0]].d+'</h5>';
                        er.appendChild(et);

                        et=document.createElement('td');
                        et.className='cine_fn_rol_i_tq';
                        et.setAttribute('id','ciq_'+k);
                        et.innerHTML='x&nbsp;'+cur[itm[0]].q+'';
                        er.appendChild(et);

                        et=document.createElement('td');
                        et.className='cine_fn_rol_i_tp';
                        et.innerHTML='$ '+cur[itm[0]].p;
                        er.appendChild(et);

                        ec.appendChild(er);

                    }
                }

            //}

            tr.appendChild(td);
            tb.appendChild(tr);

            c.appendChild(tb);
            c.appendChild(ec);

        }

        cineBoxFnModalAfterLoad(e);

    }
    // MAIN CHECK FOR CART DEFAULT INFORMATION FOR USER
    // THROWS query=restoorder & type=getorderident
    function cineBoxFnOrderCartLoad(o){

        if(typeof o==='string' && o[0]=='{'){
            o=JSON.parse(o);
            if(o.success){
                cineBoxFnRestoOrderPut(false,o);
            }
        }

    }

    /* -----------------------------------------------
     * * * * * * * * * * * * * * * * * * * * * * * * *
     ADDITIONAL FUNCTIONAL FOR ORDERS
     type = functions
     * * * * * * * * * * * * * * * * * * * * * * * *
     ------------------------------------------------ */
    function cOrderPriceCount(p,t,u){
        p=Number(p);
        t=(t==0 || typeof t==='undefined')? 8.875 : Number(t);
        u=(typeof u==='undefined')?0:Number(u);
        tax=Number(((p/100)*t).toFixed(2));
        tip=(p/100)*u;
        total=p+tax+tip;
        $cine().order.setNumbers(p,tax,tip,total,t,u);
    }

    // ----------------------------------------------------------------------------------------------------------------
    // SUPPORT FUNCTIONS FOR RESTAURANTS AND RESTAURANTS MENU
    // ----------------------------------------------------------------------------------------------------------------

    function cineBoxFnRestoIPriceCnt(d){
        var p=0,v,vc,vp=0,svp=0;
        p=Number(d.p);
        v=d.s;

        if(v && typeof v==='object'){
            for(var i in v){
                if(v.hasOwnProperty(i)){
                    vc=v[i];
                    if(vc.p>0)
                        vp+=Number(vc.p);
                    if(vc.p2>0)
                        svp+=Number(vc.p2);
                }
            }
        }

        return p+vp+svp;
    }

    // ----------------------------------------
    // ---- Restaurant menu back function -----
    // ----------------------------------------

    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this,rest);
    };

    function cineBoxFnRestoMenuBack(e,s,p,c,t){

        if(!s){

            var n,i;

            if(!p || typeof p!=='object'){

                if(cineFnWindows.length>1){
                    var last = cineFnWindows[cineFnWindows.length - 1];
                    p=document.getElementById(last);
                } else {
                    p=document.getElementById('cine_fn_view');
                }

            }

            n=document.createElement('div');
            n.className='cine_fn_resto_menu_b';
            n.setAttribute('cineident',p.id);
            i=document.createElement('div');
            i.className='cine_fn_resto_menu_b_b';
            i.innerHTML = '<div class="c-btn-back"></div>Back';
            i.addEventListener('click',function(e){cineBoxFnRestoMenuBack(e,true,this.p,this.c,this.attr)}.bind({p:p,c:c}));
            n.appendChild(i);

            if(typeof t!=='undefined'){
                i=document.createElement('div');
                i.className='cine_fn_resto_menu_b_t';
                i.innerHTML = t;
                n.appendChild(i);
            }

            p.style.display='none';
            cineFnLastWindow=p;

            return n;

        } else {

            if(c){
                cineWinCtrlRemWin(false,c);
            }
            if(p){
                var ttl=p.getAttribute('cinetitle');
                if(ttl!=null){
                    var atl=document.getElementById('cine_fn_ctitle');
                    atl.innerHTML=ttl;
                }

                p.style.display='block';

            }
        }
        return false;
    }

    function cineWinCtrlRemWin(m,w,c){
        var e,i,r,l;
        if(m){
            i=cineFnCurrentWindow.getAttribute('modalid');
            if(r=cineFnWindows.indexElm(i)){
                e=cineFnCurrentWindow;
                cineFnWindows.remove(r.pos);
                e.parentNode.removeChild(e);
                l=cineFnWindows.length;
                if(l>0){
                    cineFnCurrentWindow=document.getElementById('cinemodal'+cineFnWindows[l-1]);
                    cineFnCurrentWindow.style.display='block';
                }
                else
                {
                    p=document.getElementById('cine_fn_view');
                    p.style.display='block';
                    cineFnCurrentWindow=undefined;
                }

            }
        }
        if(w){
            i=w.getAttribute('modalid');
            if(r=cineFnWindows.indexElm(i)){
                cineFnWindows.remove(r.pos);
            }
            w.parentNode.removeChild(w);
            l=cineFnWindows.length;
            if(l>0){
                cineFnCurrentWindow=document.getElementById('cinemodal'+cineFnWindows[r.pos-1]);
                if(cineFnCurrentWindow!=null)
                    cineFnCurrentWindow.style.display='block';
                else
                {
                    cineFnCurrentWindow=document.getElementById('cinemodal'+cineFnWindows[l-1]);
                    cineFnCurrentWindow.style.display='block';
                }
            }
            else
            {
                p=document.getElementById('cine_fn_view');
                p.style.display='block';
                cineFnCurrentWindow=undefined;
            }
        }
    }

    function cineBlockButton(e){
        var c;
        if(e!=null){
            c=e.className;
            e.className+=' disabled';
            setTimeout(function () {
                var e=this.o;
                e.className=this.c;
            }.bind({o:e,c:c}),1000);
        }
    }

    /* DEPRECATED
    //function cineBoxFnShortDist(x1,y1,x2,y2){
    //    var xs = 0;
    //    var ys = 0;
    //    xs = x2 - x1;
    //    xs = xs * xs;
    //    ys = y2 - y1;
    //    ys = ys * ys;
    //    return Math.sqrt( xs + ys );
    //}
    */

    /* CLIENT SIDE FUNCTIONS */
    /*
    if(typeof Number.prototype.toRadians == 'undefined'){Number.prototype.toRadians = function() { return this * Math.PI / 180; };}
    function cineBoxFnRealDist(x1,y1,x2,y2){

        x2=Number(x2); y2=Number(y2);
        var r = 6371;
        var l1 = x1.toRadians();
        var l2 = x2.toRadians();
        var dl = (x2-x1).toRadians();
        var dt = (y2-y1).toRadians();
        var a = Math.sin(dl/2) * Math.sin(dl/2) + Math.cos(l1) * Math.cos(l2) * Math.sin(dt/2) * Math.sin(dt/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (r * c).toFixed(3);
    }
    //function cineBoxFnDevDetect(){
    //    var a='iphone'; var b='ipad'; var c='ipod'; var d='android'; var e='iemobile'; var z=-1; var u = navigator.userAgent.toLowerCase();
    //    return (u.indexOf(a)!=z||u.indexOf(b)!=z||u.indexOf(c)!=z||u.indexOf(d)!=z||u.indexOf(e)!=z);
    //}
    */
    function cineBoxLabelFrame(o){var e,i,l,k,d;
        e = document.createElement('div'); e.className='cine_fn_domframe';
        i = document.createElement('div'); i.className='cine_fn_domframe_l';
        i.innerHTML=o.title;
        e.appendChild(i);
        i = document.createElement('div'); i.className='cine_fn_domframe_c cine_list_item';
        if(o.color!=undefined){
            i.className = 'cine_fn_domframe_c';
            i.style.backgroundColor = o.color;
        }
        if(o.notpad!=undefined){
            i.className += ' nopadding';
        }
        if(o.hasOwnProperty('elems')){
            d=o.elems;

            if(d.nodeType===1){
                i.appendChild(d);
            }else{
                l=d.length;
                if(l>0){
                    for(k=0;k<l;k++){
                        if(typeof d[k]==='object')
                            i.appendChild(d[k]);
                        else
                            i.insertAdjacentHTML('beforeend',d[k]);
                    }
                }
            }
        }else{
            i.appendChild(o);
        }
        e.appendChild(i);
        return e;
    }
    function cineBoxFnDomObject(e,type){
        var obj; var ev=false;
        if(e.target){
            obj=e.target;
            if(e.preventDefault) ev=e;
        }
        else if (typeof e==='string'){
            obj=document.getElementById(e);
        } else
            obj=e;

        if(typeof type !== 'undefined'){
            if(obj.tagName == type)
                return obj;
            else
                return obj.parentNode;
        } else
            return {object:obj,event:ev};
    }
    function cineBoxFnModalAfterLoad(o){

        var cc,el,le,i,h=0;
        if(o.nodeType==1){

            cc = o.getAttribute('modalid');
            cc = document.getElementById('cinecont' + cc);
            el = o.childNodes;
            le = el.length;

            for (i = 0; i < le; i++) {
                if(el[i].className != 'cine_fn_modal_centerer')
                    h += parseInt(el[i].offsetHeight);
            }

            var dh, mw, mh, ms;
            dh = cineViewH;
            wh = cineViewW;
            fdh = dh + 76;

            mh = o.offsetHeight;
            th = mh;
            mh = (mh < fdh) ? (dh - 76) + 'px' : 'auto';
            o.style.height = mh;

            o.style.width=wh + 'px';
            o.style.maxWidth = wh + 'px';
            o.style.height = mh;
            if(cc!=null){
                cc.style.maxWidth = wh + 'px';
                cc.style.height = (dh - 76 - h) + 'px';
            }
        }

    }

    /* CONSUMER used in FIND RESTAURANTS */
    /*
    function cineCreateDomEl(t,i,c,h,p){

        var e=document.createElement(t);
        e.setAttribute('id',i);
        e.className=c;
        if(h.length>0 && e.nodeName=='INPUT'){
            e.setAttribute('value',h);
        } else {
            e.innerHTML = h;
        }
        if(typeof p==='object'){
            p.appendChild(e);
        }
        return e;
    }
    */

    function cineNotify(o){

        var l,i,c,b,a;
        if(cineFnNotify!=null){
            l=cineFnNotify;
        } else {
            l=document.createElement('div');
            l.className='c_notify_layer';
            cineFnNotify=l;
            document.body.appendChild(l);
        }

        i=document.createElement('div'); i.className='c_notify_i';

        c=document.createElement('div'); c.className='c_notify_i_c';
        c.innerHTML= o.message;

        b=document.createElement('div'); b.className='c_notify_i_b';
        a=document.createElement('button'); a.className='c_notify_b_b';
        a.innerHTML='<div class="fa fa-times" style="margin-left:-10px"></div>';
        a.addEventListener('click',function(e){cineNotifyClose(e,this)}.bind({notify:l,item:i}));

        if('destroy' in o){
            var d=o['destroy'];
            if(d>0){
                setTimeout(function(e){cineNotifyClose(e,this)}.bind({notify:l,item:i}),d*1000);
            }
        }

        b.appendChild(a);
        i.appendChild(c);
        i.appendChild(b);
        l.appendChild(i);

    }

    function cineNotifyClose(e,o){

        var i = o.item;
        var l = o.notify;
        l.removeChild(i);
        var c=l.childNodes;
        c= c.length;
        if(c<1){
            l.parentNode.removeChild(l);
            cineFnNotify=null;
        }

    }

    function cineFnClearDOM(node){
        while (node.firstChild){
            node.removeChild(node.firstChild);
        }
    }

    if(!window.$cine){window.$cine = $cine;}
})();

function cOpenTinyModal(s){

    var o=document.createElement('div');
    var u=document.createElement('div');

    var h=cineViewH/100*83;

    o.style.cssText='position:absolute; width:90%; height:'+h+'px; padding:10px; margin:90px 5% 0 5%; top:0; left:0; background:#ffffff; border-radius:8px;';
    u.style.cssText='position:absolute; width:100%; height:100%; top:0; left:0;';
    u.className='c_modal_fancy_bg';

    var f=document.createElement('div');
    f.style.cssText='width:100%; height:100%; text-align:center; position:relative;';
    o.appendChild(f);
    var r={modal:o,under:u,content:f};

    if(s.buttonShow){
        c=document.createElement('div');
        c.style.cssText='width:100%; height:50px; text-align:center; position:absolute; bottom:8px;';
        b=document.createElement('button');
        b.className="cine_fn_color_btn cine_short_btn";
        b.style.cssText='width:256px !important;';
        b.innerHTML= s.buttonName;
        b.addEventListener('click',function(e){
            if(typeof s.buttonEvent === 'function')
            {
                s.buttonEvent();
            }
            this.under.style.display='none';
            this.under.parentNode.removeChild(this.under);
            this.modal.parentNode.removeChild(this.modal);
        }.bind(r));
        c.appendChild(b);
        f.appendChild(c);
        r['button']=c;
    }

    document.body.appendChild(u);
    document.body.appendChild(o);

    return r;
}

function cTimeDST(nt,z){
    var h='',o,x=0,y=0,e;
    if(z<0)
        h+='-';
    h+='0'+Math.abs(z)+':00';
    var n=new Date(nt+h);
    var a = new Date(n.getFullYear(),0,1);
    var b = new Date(n.getFullYear(),6,1);
    var t=Math.max(a.getTimezoneOffset(),b.getTimezoneOffset());
    var no=n.getTimezoneOffset();
    if(t>no){
    //if(no>0){
        o=cineTimeTzObj[z];
        x=Math.abs(o)*60;
        if(x!=no){
            y=((no-x)*60)*1000;
        }
    } else {
        o=z;
        x=Math.abs(o)*60;
        if(x!=no){
            y=((no-x)*60)*1000;
        }
    }
    e=n.getTime();
    console.log(x);
    console.log(y);
    console.log(no);
    console.log(n.getTime());
    console.log(n.getTime()+y);
    return {offset:o,time:e+y,usertime:e};
}

function cTimeObject(s){
    if(typeof s!=='undefined') {
        var e = s.split(' ');
        var d = e[0];
        var t = e[1];
        if (typeof d !== 'undefined')
            d = d.split('-');
        else
            d = ['0000', '00', '00'];
        if (typeof t !== 'undefined') {
            t = t.split(':');
            t[2] = (typeof t[2] === 'undefined') ? '00' : t[2];
        }
        else
            t = ['00', '00', '00'];
        tMin = Number(t[1]);
        tHor = Number(t[0]);
        tSec = Number(t[2]);
        dDay = Number(d[2]);
        dMon = Number(d[1]);
        return {
            year: d[0],
            month: (dMon < 10) ? '0' + dMon : dMon,
            day: (dDay < 10) ? '0' + dDay : dDay,
            hour: (tHor < 10) ? '0' + tHor : tHor,
            minute: (tMin < 10) ? '0' + tMin : tMin,
            second: (tSec < 10) ? '0' + tSec : tSec,
            y: Number(d[0]),
            m: dMon,
            d: dDay,
            h: tHor,
            i: tMin,
            s: tSec
        };
    } return false;
}

function cineTimeNormalize(h,m){
    var nh=h%12;
    if(nh==0)nh=12;
    if(typeof m==='string')
        return nh+":"+m+(h<12?' am':' pm');
    else
        return nh+":"+(m<10?"0"+m:m)+(h<12?' am':' pm');
}